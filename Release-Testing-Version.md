## OpenHarmony 3.1.5.1版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.5.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第4轮测试，验收:    |
| L0L1:                                       |
| L2:                                |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-2-20**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220219_221247/version-Master_Version-OpenHarmony_3.1.5.1-20220219_221247-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220219_221124/version-Master_Version-OpenHarmony_3.1.5.1-20220219_221124-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220219_221047/version-Master_Version-OpenHarmony_3.1.5.1-20220219_221047-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-2-20**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows：http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220219_221317/version-Master_Version-OpenHarmony_3.1.5.1-20220219_221317-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.5.1/20220219_231339/L2-SDK-MAC.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.5.1/20220221_201100/version-Master_Version-OpenHarmony_3.1.5.1-20220221_201100-dayu200.tar.gz |

## OpenHarmony 3.1.5.1版本特性清单：

| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4NJTS](https://gitee.com/openharmony/ace_engine_lite/issues/I4NJTS) | [轻量级图形子系统]支持通用touch事件、list组件支持scrollbottom/scrolltop事件 | 轻量系统       | SIG_AppFramework         | [@piggyguy](https://gitee.com/piggyguy)               |
| 2    | [I4NJTD](https://gitee.com/openharmony/graphic_ui/issues/I4NJTD) | [轻量级图形子系统]list组件支持scrollbottom/scrolltop事件     | 轻量系统       | SIG_AppFramework         | [@piggyguy](https://gitee.com/piggyguy)               |
| 3    | [I4OGF0](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4OGF0) | 【资料】thermal_manager部件类资料需求                        | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4OEQT](https://gitee.com/openharmony/powermgr_power_manager/issues/I4OEQT) | 【集成验证】不亮屏检测                                       | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 5    | [I4P7EY](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7EY) | [语言编译运行时子系统]支持TS Runtime                         | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 6    | [I4P7F5](https://gitee.com/openharmony/third_party_musl/issues/I4P7F5) | [语言编译运行时子系统]支持LLDB命令窗口                       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 7    | [I4P7EV](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4P7EV) | [语言编译运行时子系统]支持ts/js模块化编译                    | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 8    | [I4OGD1](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGD1?from=project-issue) | 【新增特性】【DMS】支持组件间跨设备的onCall调用              | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 9   | [I4OGCY](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCY?from=project-issue) | 【新增特性】【AMS/框架】支持组件间跨设备的onCall调用         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 10   | [I4OGCX](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCX?from=project-issue) | 【新增特性】【任务管理】快照变化更新                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 11   | [I4OGCW](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCW?from=project-issue) | 【新增特性】【任务管理】任务变化更新                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 12   | [I4OGCV](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCV?from=project-issue) | 【新增特性】【任务管理】同步任务快照                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 13   | [I4OGCU](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCU?from=project-issue) | 【新增特性】【任务管理】结束任务同步                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 14   | [I4OGCT](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCT?from=project-issue) | 【新增特性】【任务管理】发起任务同步                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 15   | [I4OGCS](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCS?from=project-issue) | 【新增特性】【任务管理】查询任务快照                         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 16   | [I4OGCP](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCP?from=project-issue) | 【新增特性】【DMS】跨设备组件权限校验                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 17   | [I4OGCK](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCK?from=project-issue) | 【增强特性】框架等待分布式对象同步完成后返回迁移结果         | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 18   | [I4OH99](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH99?from=project-issue) | 【samgr】注册/查询本地系统服务权限控制                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 19   | [I4TS0Z](https://gitee.com/openharmony/distributedschedule_samgr_lite/issues/I4TS0Z?from=project-issue) | 【新增】轻量系统samgr支持远程服务管理                            | 轻量系统      | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 20   | [I4OH96](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH96?from=project-issue) | 【samgr】samgr资料                                           | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 21   | [I4OH93](https://gitee.com/openharmony/device_profile_core/issues/I4OH93?from=project-issue) | 【device_profile】同步功能适配可信群组                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 22   | [I4OGD7](https://gitee.com/openharmony/device_profile_core/issues/I4OGD7?from=project-issue) | 【device_profile】deviceProfile资料                          | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 23   | [I4PBBV](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBBV) | 【新增特性】事件耗时调用                                     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 24   | [I4PD0O](https://gitee.com/openharmony/notification_ans_standard/issues/I4PD0O) | 【特性增强】通知发送使能能力增强                             | 事件通知子系统 | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 25   | [I4PBQ1](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBQ1) | 【增强特性】分布式通知能力支持dump命令                       | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 26   | [I4PBR0](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBR0) | 【新增特性】支持其他设备的通知点击后在跨设备跳转             | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 27  | [I4PC2S](https://gitee.com/openharmony/notification_ces_standard/issues/I4PC2S) | 【新增特性】公共事件支持多用户特性                           | 事件通知子系统 | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 28  | [I4PCHK](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHK) | 【新增特性】C++/JS 客户端与服务端互通                        | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 29 | [I4PCS0](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS0) | 【新增特性】提供Ability启动停止方法                          | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 30  | [I4PQ5F](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5F) | 【资料】提供ZIDL部件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 31  | [I4PQ5N](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5N) | 【增强特性】支持IAbilityController                           | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 32  | [I4PCNK](https://gitee.com/openharmony/aafwk_standard/issues/I4PCNK) | 【资料】提供运行管理新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 33  | [I4PCPA](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPA) | 【资料】提供环境变化新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 34  | [I4PCV0](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV0) | 【资料】提供通用组件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 35  | [I4PQ5X](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ5X) | 【增强特性】Extension接口整改                                | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 36  | [I4PCLY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLY) |【新增特性】对外接口权限校验验收|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 37  | [I4PCPI](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPI) |【增强特性】支持系统环境查询|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 38  | [I4PCRG](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRG) |【新增特性】提供Ability监听器|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 39 | [I4PCRL](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRL) |【新增特性】测试框架整体功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 40  | [I4PCRO](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRO) |【新增特性】测试框架需要提供如下查询相关的功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 41  | [I4PCRQ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRQ) |【新增特性】提供调度组件生命周期相关的功能|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 42  | [I4PCRX](https://gitee.com/openharmony/aafwk_standard/issues/I4PCRX) |【新增特性】测试框架提供可以执行shell命令能力|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 43  | [I4PCS6](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS6) |【新增特性】AA适配测试框架|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 44  | [I4PCVU](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVU) |【新增特性】通用组件call调用|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 45  | [I4PKYA](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYA?from=project-issue) | 【新增特性】包命令行工具                                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 46  | [I4PKYS](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYS?from=project-issue) | 【新增特性】支持现有js查询接口的权限管控                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 47  | [I4PKYW](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYW?from=project-issue) | 【新增特性】支持现有js监听安装、卸载、更新状态变化接口的权限管控 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 48  | [I4PKYX](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYX) | 【新增特性】支持现有js清理缓存接口的权限管控                 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 49 | [I4PKYY](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYY) | 【新增特性】支持现有js使能/禁用应用/组件接口的权限管控       | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 50  | [I4PKYZ](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYZ) | 【新增特性】返回应用类型                                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 51  | [I4PKZ1](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKZ1) | 【新增特性】应用数据目标标签设置                             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 52  | [I4PKYT](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYT) | 【新增特性】支持现有js安装接口的权限管控 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 53  | [I4QA3D](https://gitee.com/openharmony/appexecfwk_standard/issues/I4QA3D?from=project-issue) | 【增强特性】新增zlib解压、压缩数据native接口 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 54  | [I4SY5T](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5T?from=project-issue) | 【SysCap】包管理子系统支持SysCap机制 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 55  | [I4Q9S6](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9S6)  | [内核子系统]【增强特性】支持内核接口融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 56 | [I4QESV](https://gitee.com/openharmony/device_manager/issues/I4QESV)  | 【新增特性】设备可用状态上报性能跟踪   | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)           |
| 57   | [I4QEKZ](https://gitee.com/openharmony/drivers_framework/issues/I4QEKZ) | 【新增特性】支持用户态平台驱动接口                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 58 | [I4QEL1](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL1) | 【新增特性】【新增特性】支持基于FB显示架构的基础功能                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 59  | [I4QEL2](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEL2) | 【增强特性】马达驱动模型能力增强                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 60   | [I4QT3S](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3S) | 【新增特性】长时任务管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 61   | [I4QT3T](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3T) | 【新增特性】长时任务规范 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 62   | [I4QT3U](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3U) | 【新增特性】长时任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 63   | [I4QT3V](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3V) | 【新增特性】长时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 64   | [I4QT3W](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3W) | 【新增特性】长时任务申请/注销 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 65   | [I4QU0O](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0O) | 【新增特性】设备和系统相关状态检测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 66   | [I4QU0P](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0P) | 【新增特性】延迟任务调度 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 67   | [I4QU0R](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0R) | 【新增特性】延迟任务后台管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 68   | [I4QU0S](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0S) | 【新增特性】延迟任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 69  | [I4QU0T](https://gitee.com/openharmony/resourceschedule_work_scheduler/issues/I4QU0T) | 【新增特性】延迟任务适配PC多账户 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 70   | [I4U076](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4U076) | 【SysCap】全局资源调度管控子系统支持SysCap机制 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 71  | [I4R2Y1](https://gitee.com/openharmony/global_cust_lite/issues/I4R2Y1) | 【增强特性】提供定制框架js接口                               | 标准系统 | SIG_ApplicationFramework          | [@zhengbin5](https://gitee.com/zhengbin5)         |
| 72  | [I4R2Y8](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2Y8) | 【新增特性】支持颜色模式限定词                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 73  | [I4R2YA](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YA) | 【新增特性】新增资源管理NDK接口                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 74  | [I4R2YC](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YC) | 【新增特性】资源overlay                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 75  | [I4R2MP](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2MP) | 【新增特性】时区数据编译                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 76  | [I4R2LF](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2LF) | 【新增特性】时区数据解析                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 77  | [I4R3FR](https://gitee.com/openharmony/global_i18n_standard/issues/I4R3FR) | 【资料】全球化新增API资料更新                       | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 78  | [I4RD3H](https://gitee.com/openharmony/kernel_liteos_m/issues/I4RD3H?from=project-issue) | 【liteos_m部件】增加支持POSIX接口                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5) |
| 79  | [I4QEKY](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKY) | 【新增特性】Audio支持模拟耳机设备                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
|  80  | [I4RTYX](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYX?from=project-issue) | 【新增特性】【接口】提供查询本机或者组网内其它设备的设备安全等级信息的接口 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  81  | [I4RTYW](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYW?from=project-issue) | 【新增特性】【服务】支持获取自己或者组网内其它设备的设备安全等级信息 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  82  | [I4RTYU](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYU?from=project-issue) | 【新增特性】【服务】支持被组网内其它设备查询自己的设备安全等级信息 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
|  83  | [I4RTYN](https://gitee.com/openharmony/security_device_security_level/issues/I4RTYN?from=project-issue) | 【新增特性】【南向】支持OEM厂家接入设备安全等级模块 | 标准系统 | SIG_Security | [@zhirenx](https://gitee.com/zhirenx) |
| 84 | [I4SY5E](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5E?from=project-issue) | 【packing_tool部件】支持打包工具将syscap二进制文件打包到hap包中 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 85 | [I4SY5G](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SY5G?from=project-issue) | 【bundle_manager部件】支持基于SysCap的应用安装 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 86    | [I4TNBV](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBV) | 【新增规格】进程启动配置能力增强                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 87    | [I4TNBL](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBL) | 【新增特性】支持UHDF类进程按需启动                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 88    | [I4TNBQ](https://gitee.com/openharmony/startup_init_lite/issues/I4TNBQ) | 【SysCap】启动恢复子系统支持SysCap机制                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 89    | [I4HAMD](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMD) | 【data_share_ability】支持对数据访问方式的控制                    | 标准系统   | SIG_DataManagement | [@verystone](https://gitee.com/verystone)         |
| 90    | [I4TJFZ](https://gitee.com/openharmony/security_deviceauth/issues/I4TJFZ) | 【增强特性】DeviceAuth部件支持设备间互信关系认证的多用户隔离，使用指定系统用户下管理的互信关系进行认证                   | 标准系统   | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 91    | [I4TJG1](https://gitee.com/openharmony/security_deviceauth/issues/I4TJG1) | 【增强特性】DeviceAuth部件实现互信群组数据多实例，支持指定用户的数据查询                    | 标准系统   | SIG_Security | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 92    | [I4TSJE](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSJE) |  【新增规格】【face_auth】支持用户本地人脸录入   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 93    | [I4TSJY](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSJY) |  【新增规格】【face_auth】支持用户本地人脸认证   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 94    | [I4TSK7](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSK7) |  【新增规格】【face_auth】支持用户本地人脸删除   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 95    | [I4TSKP](https://gitee.com/openharmony/useriam_faceauth/issues/I4TSKP) |  【需求描述】独立需求跟踪人脸认证框架DFX相关需求   | 标准系统   | SIG_Security      | [@tianshi_liu](https://gitee.com/tianshi_liu)     |
| 96    | [I4SAI0](https://gitee.com/openharmony/security_dataclassification/issues/I4SAI0) |  【新增特性】提供DataTransitMgrLib部件，支持数据跨设备流转时的管控策略   | 标准系统   | SIG_Security      | [@wangyongzhong2](https://gitee.com/wangyongzhong2)     |
| 97   | [I4H4FH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4H4FH)|【distributed_kv_store】分布式数据库支持分类分级|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 98   | [I4H3M8](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3M8)|【新增特性】分布式数据对象支持复杂类型|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 99   | [I4HAMD](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMD)|【data_share_ability】支持对数据访问方式的控制|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 100   | [I4PO00](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4PO00)|【分布式RDB】数据同步需求|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 101   | [I4OTW6](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4OTW6)|【distributed_kv_store】分布式数据库Query支持InKeys谓词|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 102   | [I4TXSP](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4TXSP)|【SysCap】分布式数据管理子系统支持SysCap机制|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 103   | [I4TTK5](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTK5)|【新增特性】支持磁盘管理查询的特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 104   | [I4TTJV](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJV)|【新增特性】支持卷信息查询和管理特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 105   | [I4TTJN](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJN)|【新增特性】支持外卡设备相关事件分发特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 106   | [I4TTJJ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTJJ)|【资料】storage_manag   er部件资料需求|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 107   | [I4TTHQ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTHQ)|【新增特性】支持外部存储访问需求|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 108   | [I4TTHF](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTHF)|【新增特性】支持外卡上下线管理特性|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 109   | [I4TTGR](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTGR)|【新增特性】【storage_manager部件】文件加密特性使能|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 110   | [I4TTH9](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TTH9)|【新增特性】【storage_manager部件】CE密钥生命周期管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 111   | [I4TT7Y](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4TT7Y)|【新增特性】【storage_manager部件】文件加密特性对外接口|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 112   | [I4SNSU](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4SNSU)|【新增特性】支持应用沙箱隔离能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 113   | [I4TTM7](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTM7)|【新增特性】支持设备间的sendfile能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 114   | [I4TTOU](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOU)|【新增特性】支持提供用户态跨设备分享接口能力规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 115   | [I4TTOP](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOP)|【新增特性】支持建立本地文件与分布式文件的映射规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 116   | [I4TTOJ](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOJ)|【新增特性】为数据标签访问提供应用接口|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 117   | [I4TTOF](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTOF)|【新增特性】支持异账号能力规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 118   | [I4TTNG](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTNG)|【新增特性】支持数据分类设备分级，控制数据流转规格|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 119   | [I4TTN8](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTN8)|【新增特性】支持分布式文件系统的基础功能|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 120   | [I4TTMN](https://gitee.com/openharmony/filemanagement_dfs_service/issues/I4TTMN)|【新增特性】支持应用包名级权限配置|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 121 | [I4TT8L](https://gitee.com/openharmony/security_huks/issues/I4TT8L) |【新增规格】HUKS提供三段式密钥管理接口|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 122 | [I4TT8P](https://gitee.com/openharmony/security_huks/issues/I4TT8P)|【新增规格】HUKS 架构优化，提供抽象统一的HAL层。|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 123 | [I4TT8Z](https://gitee.com/openharmony/security_huks/issues/I4TT8Z)|【新增规格】HUKS支持多用户隔离|安全子系统|SIG_Security|[@chaos-liang](https://gitee.com/Chaos-Liang)|
| 124 | [I4S5RB](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5RB) |【新增特性】PKI应用签名工具支持生成Profile签名调试/发布证书|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 125 | [I4S5R7](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5R7) |【新增特性】PKI应用签名工具支持生成应用调试/发布证书|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 126 | [I4S5RC](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5RC) |【新增特性】PKI应用签名工具支持Profile文件签名|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 127 | [I4S5QZ](https://gitee.com/openharmony/developtools_hapsigner/issues/I4S5QZ) |【新增特性】PKI应用签名工具支持Hap包签名|安全子系统| SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)|
| 128   | [I4U08D](https://gitee.com/openharmony/build/issues/I4U08D) | [语言编译运行时]【SysCap】语言编译器运行时子系统支持SysCap  | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 129   | [I4P7F7](https://gitee.com/openharmony/third_party_musl/issues/I4P7F7) | [语言编译运行时，图形图像，DRF]NDK整体集成 | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 130   | [I4QQ1E](https://gitee.com/openharmony/ark_runtime_core/issues/I4QQ1E) | [语言编译运行时]【新增规格】Pandafile模块中zlib替换miniz | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 131   | [I4TMTO]() | [语言编译运行时]【c_cpp_runtime部件】动态库卸载 | 标准系统 | SIG_CompileRuntime       | [@huanghuijin]()         |
| 132   | [I4U73N](https://gitee.com/openharmony/filemanagement_file_api/issues/I4U73N)|【SysCap】文件管理子系统支持SysCap机制|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 133   | [I4Q6AQ](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AQ)|【【新增特性】Watchdog机制标准系统|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 134   | [I4Q6AP](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AP)|【新增特性】支持JS app性能分析信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 135   | [I4Q6AN](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AN)|【新增特性】：命令行调试通道|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 136   | [I4PJE6](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4PJE6)|【【新增特性】支持JS app内存信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 137   | [I4PJE5](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4PJE5)|【新增特性】支持JS app native内存信息调试调优能力|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 138   | [I4PJDO](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDO)|【新增特性】hungtask卡死-内核hungtask监控D状态进程|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 139   | [I4PJDN](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDN)|【新增特性】SCREEN ON卡死-不亮屏故障卡死|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 140   | [I4PJDP](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDP)|【新增特性】Power按键-长按、短按Power按键事件上报|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 141   | [I4PJDM](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4PJDM)|【新增特性】INIT卡死-init长时间不执行|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 142   | [I4Q6AR](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AR)|【新增特性】鸿蒙芯片维测框架|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 143   | [I4Q6AZ](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6AZ)|【资料】hilog部件的js api需求，kmsg支持需求，init日志支持需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 144   | [I4Q6B2](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B2)|【新增特性】hilog支持开机阶段的日志打印和保存|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 145   | [I4Q6B4](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B4)|【新增特性】hilog支持js接口|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 146   | [I4Q6BA](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6BA)|【增强特性】支持HiLog关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 147   | [I4Q6B9](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B9)|【增强特性】支持HiSysEvent关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 148   | [I4Q6B7](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B7)|【增强特性】支持HiAppEvent关联traceid|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 149   | [I4Q6B6](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4Q6B6)|【增强特性】支持HiTrace JS接口|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 150   | [I4U0JZ](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4U0JZ)|【新增特性】供鸿蒙hisysevent系统事件管理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 151   | [I4PUKP](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4PUKP)|【新增特性】hisysevent订阅接口支持TAG方式订阅事件|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 152   | [I4U0KL](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4U0KL)|【SysCap】DFX子系统支持SysCap机制|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 153   | [I4U0KP](https://gitee.com/openharmony/developtools_profiler/issues/I4U0KP)|【profiler部件】cpu profiler功能|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 154   | [I4U89V](https://gitee.com/openharmony/xts_acts/issues/I4U89V)|【新增】驱动/输入驱动XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 155   | [I4U8I3](https://gitee.com/openharmony/xts_acts/issues/I4U8I3)|【新增】智能开关面板样例|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 156   | [I4U8KB](https://gitee.com/openharmony/xts_acts/issues/I4U8KB)|【新增】驱动/显示驱动适配|轻量系统|SIG_Driver|[@zianed](https://gitee.com/zianed)|
| 157   | [I4U8LZ](https://gitee.com/openharmony/xts_acts/issues/I4U8LZ)|【新增】驱动/显示驱动XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 158   | [I4U8N4](https://gitee.com/openharmony/xts_acts/issues/I4U8N4)|【新增】驱动/输入驱动适配|轻量系统|SIG_Driver|[@zianed](https://gitee.com/zianed)|
| 159   | [I4U8O1](https://gitee.com/openharmony/xts_acts/issues/I4U8O1)|【新增】芯片架构SoC与Board解耦端到端联调|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 160   | [I4U8VP](https://gitee.com/openharmony/xts_acts/issues/I4U8VP)|【新增】编译构建/Module rules 编译构建流程适配SoC与Board分离机制|轻量系统|SIG_Kernel|[@kkup180](https://gitee.com/kkup180)|
| 161   | [I4U8WL](https://gitee.com/openharmony/xts_acts/issues/I4U8WL)|【新增】硬件芯片/hisilico跟随SoC与Board分离机制进行调整|轻量系统|SIG_DevBoard|[@northking-super](https://gitee.com/northking-super)|
| 162   | [I4S878](https://gitee.com/openharmony/xts_acts/issues/I4S878)|【新增】用户程序框架/轻设备包管理 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 163   | [I4TY9D](https://gitee.com/openharmony/ace_engine_lite/issues/I4TY9D)|【新增】 轻量系统图形/UI组件和图形基础组件 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 164   | [I4TFTB](https://gitee.com/openharmony/drivers_framework/issues/I4TFTB)|【关联】轻量系统新增HCS宏式解析接口|轻量系统|SIG_Driver|[@fx_zhang](https://gitee.com/fx_zhang)|
| 165   | [I4T1JA](https://gitee.com/openharmony/xts_acts/issues/I4T1JA)|【新增】轻量系统分布式软总线/分布式软总线 XTS用例|轻量系统|compatibility|[@jiyong](https://gitee.com/jiyong)|
| 166   | [I4TS0Z](https://gitee.com/openharmony/distributedschedule_samgr_lite/issues/I4TS0Z)|【新增】轻量系统samgr支持远程服务管理|轻量系统|SIG_DistributedSchedule|[@zjucx](https://gitee.com/zjucx)|



## OpenHarmony 3.1.3.5版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.3.5 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第4轮测试，验收:    |
| L0L1:                                       |
| L2:                                |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-1-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220129_202253/version-Master_Version-OpenHarmony_3.1.3.5-20220129_202253-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220129_202242/version-Master_Version-OpenHarmony_3.1.3.5-20220129_202242-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220129_202340/version-Master_Version-OpenHarmony_3.1.3.5-20220129_202340-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-1-30**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220129_202311/version-Master_Version-OpenHarmony_3.1.3.5-20220129_202311-ohos-sdk.tar.gz|
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.3.5/20220129_203628/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220130_193126/version-Master_Version-OpenHarmony_3.1.3.5-20220130_193126-hispark_taurus_L2.tar.gz|
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.5/20220130_193317/version-Master_Version-OpenHarmony_3.1.3.5-20220130_193317-dayu200.tar.gz |


**需求列表:**
| no   | issue                                                        | feture description                           | platform       | sig                      | owner                                             |
| :--- | ------------------------------------------------------------ | :------------------------------------------- | :------------- | :----------------------- | :------------------------------------------------ |
| 1    | [I4P7F1](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7F1) | [语言编译运行时子系统]方舟支持列号显示       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)     |
| 2    | [I4PC3R](https://gitee.com/openharmony/aafwk_standard/issues/I4PC3R) | 【新增特性】提供卡片开发基础能力             | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181) |
| 3    | [I4PCGJ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCGJ) | 【资料】提供卡片框架新增/增强特性资料说明    | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181) |
| 4    | [I4PCO3](https://gitee.com/openharmony/aafwk_standard/issues/I4PCO3) | 【新增特性】主线程EventRunner耗时检测        | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)     |
| 5    | [I4PPZF](https://gitee.com/openharmony/aafwk_standard/issues/I4PPZF) | 【增强特性】卡片框架适配配置文件变更         | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 6    | [I4PQ0I](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0I) | 【增强特性】获取启动参数                     | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 7    | [I4PQ0K](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0K) | 【增强特性】扩展Extension独立进程运行        | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 8    | [I4PQ0M](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0M) | 【增强特性】上下文提供消息发送和监听能力     | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 9    | [I4PQ0Z](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ0Z) | 【新增特性】支持导出客户端应用信息           | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 10   | [I4PQ12](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ12) | 【新增特性】支持导出AMS信息                  | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 11   | [I4PQ13](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ13) | 【增强特性】上下文提供权限校验及权限申请接口 | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 12   | [I4PQ19](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ19) | 【新增特性】支持桌面进程异常恢复             | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 13   | [I4PQ1E](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ1E) | 【增强特性】支持常驻进程异常恢复             | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)             |
| 14   | [I4PQ1O](https://gitee.com/openharmony/aafwk_standard/issues/I4PQ1O) | 【新增特性】支持NewWant                      | 元能力子系统   | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)           |
| 15   | [I4PKYG](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYG) | 【新增特性】支持关键流程hitrace              | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 16   | [I4PKYK](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYK) | 【增强特性】启动扫描                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 17   | [I4SIH9](https://gitee.com/openharmony/appexecfwk_standard/issues/I4SIH9) | 【新增特性】应用申请权限管理                 | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)           |
| 18   | [I4PBSZ](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSZ) | 【新增特性】根据设备状态决策通知是否提醒     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)           |
| 19  | [I4QESE](https://gitee.com/openharmony/device_manager/issues/I4QESE)  | 【新增特性】PIN码认证过程中的界面实现    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)
| 20  | [I4QESK](https://gitee.com/openharmony/device_manager/issues/I4QESK)  | 【部件化专项】【device_manager部件】device_manager部件标准化   | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)
| 21    | [I4QEKW](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKW) | 【新增特性】提供codec设备驱动模型，支持codec类型设备                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 22    | [I4QEKX](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKX) | 【特性增强】pipeline模块能力增强                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 23    | [I4QT44](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QT44) | 【新增特性】短时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 24   | [I4R2CV](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2CV) | 【增强特性】大小写转换                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 25   | [I4R2X9](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2X9) | 【部件化专项】全球化子系统部件标准化                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 26   | [I4R3CG](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R3CG) | 【新增特性】资源预览优化                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 27   | [I4OH95](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH95?from=project-issue) | 【部件化专项】系统服务管理子系统部件标准化                   | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 28   | [I4RD2M](https://gitee.com/openharmony/kernel_liteos_m/issues/I4RD2M?from=project-issue) | 【部件化专项】LiteOS-M内核部件标准化                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)
| 29   | [I4RD2U](https://gitee.com/openharmony/kernel_liteos_a/issues/I4RD2U?from=project-issue) | 【部件化专项】LiteOS-A内核部件标准化                  | 轻内核子系统       | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)
| 30   | [I4R6SK](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4R6SK)|【部件化专项】【key_value_store部件】key_value_store部件标准化|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 31    | [I4QEKQ](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKQ) | 【新增特性】Display的Gralllo、Gfx和Device的HDI接口实现服务                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 32    | [I4QEKS](https://gitee.com/openharmony/drivers_framework/issues/I4QEKS) | 【新增特性】platform_driver部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 33    | [I4QEKT](https://gitee.com/openharmony/drivers_framework/issues/I4QEKT) | 【新增特性】peripheral_driver部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 34    | [I4QEKU](https://gitee.com/openharmony/drivers_framework/issues/I4QEKU) | 【新增特性】driver_framework部件标准化                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 35  | [I4RTX4](https://gitee.com/openharmony/security_device_security_level/issues/I4RTX4)              | 【部件化专项】设备安全等级管理（DSLM）部件标准             | 标准系统        | SIG_Security                      | [@zhirenx](https://gitee.com/zhirenx)                    |
| 36   | [I4P7EX](https://gitee.com/openharmony/ark_js_runtime/issues/I4P7EX) | [语言编译运行时子系统]DFX维测支持运行时支持DFX/在异常时需要hook机制以及栈列信息特性 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 37   | [I4RFWF](https://gitee.com/openharmony/security_deviceauth/issues/I4RFWF) | 【部件化专项】【deviceauth部件】deviceauth部件标准化 | 标准系统       | SIG_Security       | [@lvyuanmin](https://gitee.com/lvyuanmin)         |
| 38   | [I4S36A](https://gitee.com/openharmony/js_util_module/issues/I4S36A) | [语言编译运行时子系统]【部件化专项】ts_js_common_api部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
|  39  | [I4S36C](https://gitee.com/openharmony/third_party_musl/issues/I4S36C) | [语言编译运行时子系统]【部件化专项】cpp_compiler_toolchain部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 40   | [I4S36D](https://gitee.com/openharmony/ark_js_runtime/issues/I4S36D) | [语言编译运行时子系统]【部件化专项】ark_ide_support部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 41   | [I4S36F](https://gitee.com/openharmony/ark_ts2abc/issues/I4S36F) | [语言编译运行时子系统]【部件化专项】ark_frontend_compiler部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 42   | [I4S36G](https://gitee.com/openharmony/ark_js_runtime/issues/I4S36G) | [语言编译运行时子系统]【部件化专项】ark_runtime部件标准化 | 标准系统     | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 43    | [I4RXK2](https://gitee.com/openharmony/startup_init_lite/issues/I4RXK2) | 【新增特性】支持为进程代持fd                      | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 44    | [I4RXJM](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJM) | 【新增特性】支持进程根据热插拔事件按需启动                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 45    | [I4RXJ9](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJ9) | 【新增特性】支持socket类进程按需启动                               | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 46    | [I4RXJ2](https://gitee.com/openharmony/startup_init_lite/issues/I4RXJ2) | 【新增规格】统一init维护命令                                | 标准系统 | SIG_BscSoftSrv       | [@xionglei6](https://gitee.com/xionglei6)     |
| 47    | [I4S6QC](https://gitee.com/openharmony/useriam_user_auth/issues/I4S6QC) | 【资料】用户IAM子系统介绍资料需求                                | 用户IAM子系统 | SIG_Security       | [@wangxu](https://gitee.com/wangxu43)     |
| 48   | [I4LRGQ](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4LRGQ)  | [内核子系统]【新增特性】OpenHarmony内核基线使能     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 49   | [I4RU58](https://gitee.com/openharmony/security_dataclassification/issues/I4RU58)   | 【部件化专项】【data_transit_mgr_lib部件】data_transit_mgr_lib部件标准化  | 标准系统   | SIG_Security  | [@wangyongzhong2](https://gitee.com/wangyongzhong2)  |

## OpenHarmony 3.1.3.3版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.3.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第3轮测试，验收:    |
| L0L1: 不涉及                                       |
| L2:                                |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-1-20**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165641/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165641-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165549/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165549-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165605/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165605-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-1-20**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_192244/version-Master_Version-OpenHarmony_3.1.3.3-20220120_192244-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.3.3/20220120_233918/L2-SDK-MAC.tar.gz  |
| hi3516dv300-L2版本： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165824/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165824-hispark_taurus_L2.tar.gz
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.3/20220120_165650/version-Master_Version-OpenHarmony_3.1.3.3-20220120_165650-dayu200.tar.gz |

**已解决的ISSUE单列表：**
无

**需求列表:**
| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4GYCD](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYCD) | 【新增特性】支持软件耗电统计                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I4GYCN](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYCN) | 【新增特性】支持硬件耗电统计                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4GYDQ](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4GYDQ) | 【新增特性】支持耗电详情记录                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4GY9U](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GY9U) | 【新增特性】支持内核温控服务                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 5    | [I4GYAF](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GYAF) | 【新增特性】支持用户层和服务温控服务                         | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 6    | [I4P7FB](https://gitee.com/openharmony/third_party_musl/issues/I4P7FB) | [语言编译运行时子系统]提供NDK中HOS与OHOS两种target，ABI的clang/llvm编译工具链 提供NDK中调试工具链，支持lldb，asan等功能 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 7    | [I4P7F2](https://gitee.com/openharmony/third_party_musl/issues/I4P7F2) | [语言编译运行时子系统]支持基于lldb的断点调试                 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 8    | [I4P7F3](https://gitee.com/openharmony/third_party_musl/issues/I4P7F3) | [语言编译运行时子系统]支持C/C++应用调试栈/变量的查看能力     | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 9    | [I4P7EQ](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7EQ) | [语言编译运行时子系统]ts类型信息提取                         | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 10   | [I4P7ER](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7ER) | [语言编译运行时子系统]Ts2abc中的类型信息增强                 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 11   | [I4P7ET](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7ET) | [语言编译运行时子系统]Panda file中的类型系统存储             | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 12   | [I4P7EU](https://gitee.com/openharmony/ark_ts2abc/issues/I4P7EU) | [语言编译运行时子系统]abc支持列号信息                        | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 13  | [I4P7FY](https://gitee.com/openharmony/js_util_module/issues/I4P7FY) | [语言编译运行时子系统]container特性/LightWeightMap接口规格   | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 14  | [I4P7FX](https://gitee.com/openharmony/js_util_module/issues/I4P7FX) | [语言编译运行时子系统]container特性/Deque接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 15  | [I4P7FV](https://gitee.com/openharmony/js_util_module/issues/I4P7FV) | [语言编译运行时子系统]container特性/HashSet接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 16  | [I4P7FT](https://gitee.com/openharmony/js_util_module/issues/I4P7FT) | [语言编译运行时子系统]container特性/TreeSet接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 17  | [I4P7FR](https://gitee.com/openharmony/js_util_module/issues/I4P7FR) | [语言编译运行时子系统]container特性/TreeMap接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 18  | [I4P7FP](https://gitee.com/openharmony/js_util_module/issues/I4P7FP) | [语言编译运行时子系统]container特性/Queue接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 19 | [I4P7FO](https://gitee.com/openharmony/js_util_module/issues/I4P7FO) | [语言编译运行时子系统]container特性/Vector接口规格           | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 20  | [I4P7FM](https://gitee.com/openharmony/js_util_module/issues/I4P7FM) | [语言编译运行时子系统]container特性/PlainArray接口规格       | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 21  | [I4P7FL](https://gitee.com/openharmony/js_util_module/issues/I4P7FL) | [语言编译运行时子系统]container特性/ArrayList接口规格        | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 22 | [I4P7FK](https://gitee.com/openharmony/js_util_module/issues/I4P7FK) | [语言编译运行时子系统]container特性/LightWeightSet接口规格   | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 23 | [I4P7FH](https://gitee.com/openharmony/js_util_module/issues/I4P7FH) | [语言编译运行时子系统]container特性/HashMap接口规格          | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 24  | [I4P7FF](https://gitee.com/openharmony/js_util_module/issues/I4P7FF) | [语言编译运行时子系统]container特性/List接口规格             | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 25 | [I4P7FE](https://gitee.com/openharmony/js_util_module/issues/I4P7FE) | [语言编译运行时子系统]container特性/Stack接口规格            | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 26  | [I4OGCO](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCO?from=project-issue) | 【新增特性】【DMS】提供跨设备迁移接口                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 27  | [I4OGCL](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCL?from=project-issue) | 【增强特性】【框架】迁移数据保存                             | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 28  | [I4OH9B](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH9B?from=project-issue) | 【samgr】动态加载未启动的本地系统服务                        | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 29 | [I4OH9A](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH9A?from=project-issue) | 【samgr】系统服务启动性能跟踪                                | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 30  | [I4OH98](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4OH98?from=project-issue) | 【samgr】SAMGR异常恢复                                       | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 31  | [I4OH94](https://gitee.com/openharmony/device_profile_core/issues/I4OH94?from=project-issue) | 【device_profile】校验DP客户端访问profile记录的权限          | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 32  | [I4OGD6](https://gitee.com/openharmony/device_profile_core/issues/I4OGD6?from=project-issue) | 【部件化专项】分布式DeviceProfile子系统部件标准化            | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 33  | [I4PBJF](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBJF) | 【部件化专项】distributed_notification_service部件标准化     | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 34  | [I4PBNF](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBNF) | 【部件化专项】common_event部件标准化                         | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 35  | [I4PBO1](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBO1) | 【资料】通知能力开发者材料                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 36  | [I4PBPM](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBPM) | 【增强特性】分布式通知支持流控                               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 37  | [I4PBRM](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBRM) | 【新增特性】支持其他设备的通知点击后在本设备跳转             | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 38  | [I4PBRW](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBRW) | 【新增特性】支持设备级的分布式通知使能控制                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 39 | [I4PBSE](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSE) | 【新增特性】支持通知管理应用设置和查询应用级的分布式通知使能 | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 40  | [I4PBSP](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBSP) | 【新增特性】支持应用设置分布式通知能力是否使能               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 41  | [I4PBT7](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBT7) | 【新增特性】分布式通知同步                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 42 | [I4PBU3](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBU3) | 【新增特性】分布式通知联动取消                               | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 43 | [I4PBUU](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBUU) | 【新增规格】 支持通过config.json静态配置公共事件，支持通过wokscheduler静态拉起订阅者 | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 44 | [I4PBV9](https://gitee.com/openharmony/notification_ces_standard/issues/I4PBV9) | 【新增规格】 支持静态订阅者管控                              | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 45 | [I4PCH4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCH4) | 【新增特性】卡片支持多用户                                   | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 46 | [I4PCHC](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHC) | 【元能力-运行管理部件化】基于SysCap的平台代码解耦和部件化改造 | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 47 | [I4PCHF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCHF) | 【元能力-卡片管理部件化】基于SysCap的平台代码解耦和部件化改造 | 元能力子系统   | SIG_ApplicationFramework | [@lsq1474521181](https://gitee.com/lsq1474521181)     |
| 48  | [I4PPVU](https://gitee.com/openharmony/aafwk_standard/issues/I4PPVU) | 【新增特性】进程多实例                                       | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 49 | [I4PPW2](https://gitee.com/openharmony/aafwk_standard/issues/I4PPW2) | 【资料】提供测试框架新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 50  | [I4PCLO](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLO) | 【增强特性】Ability多实例                                    | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 51  | [I4PCM1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM1) | 【新增特性】提供ce/de级上下文                                | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 52  | [I4PCOQ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCOQ) | 【新增特性】应用管理                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 53  | [I4PCS2](https://gitee.com/openharmony/aafwk_standard/issues/I4PCS2) | 【资料】提供测试工具新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 54  | [I4PCVN](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVN) | 【新增特性】支持任务快照获取和更新                           | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 55  | [I4PCWF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCWF) | 【资料】提供服务组件新增/增强特性资料说明                    | 元能力子系统   | SIG_ApplicationFramework | [@sheilei](https://gitee.com/sheilei)                 |
| 56  | [I4PPW6](https://gitee.com/openharmony/aafwk_standard/issues/I4PPW6) | 【增强特性】指定窗口模式启动组件                             | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 57  | [I4PPWA](https://gitee.com/openharmony/aafwk_standard/issues/I4PPWA) | 【增强特性】Ability框架适配配置文件变更                      | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 58 | [I4PPWD](https://gitee.com/openharmony/aafwk_standard/issues/I4PPWD) | 【增强特性】Extension框架适配配置文件变更                    | 元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 59 | [I4PKY3](https://gitee.com/openharmony/aafwk_standard/issues/I4PKY3) | 【部件化专项】bundle_manager部件标准化                       | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 60  | [I4PKYB](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYB) | 【增强特性】schema适配配置文件重构                           | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 61  | [I4PKYD](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYD) | 【新增特性】安装能力适配config.json调整                      | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 62  | [I4PKYF](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYF) | 【新增特性】支持查询指定Metadata资源profile配置文件的信息    | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 63  | [I4PKYH](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYH) | 【新增特性】支持对Extension的查询                            | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 64  | [I4PKYI](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYI) | 【新增特性】提供清除数据的能力                               | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 65  | [I4PKYR](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYR) | 【新增特性】系统定义权限的初始化                             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 66 | [I4PKYU](https://gitee.com/openharmony/aafwk_standard/issues/I4PKYU) | 【新增特性】支持对应用权限信息的查询                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 67 | [I4PXN0](https://gitee.com/openharmony/build/issues/I4PXN0)  | [编译构建子系统]【增强特性】编译构建日志优化，日志按级别显示               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 68 | [I4PXND](https://gitee.com/openharmony/build/issues/I4PXND)  | [编译构建子系统]【增强特性】hb命令安装、集成及扩展支持               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 69 | [I4PKY7](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY7) | 【新增特性】跨设备信息同步 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 70 | [I4PKY8](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKY8) | 【新增特性】跨设备信息查询 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 71 | [I4PKYE](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYE) | 【新增特性】支持查询禁用的组件信息和应用信息 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 72 | [I4QA2V](https://gitee.com/openharmony/appexecfwk_standard/issues/I4QA2V) | 【部件化专项】bundle_tool部件标准化 | 包管理子系统 | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao) |
| 73 | [I4Q9RC](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9RC)  | [内核子系统]【增强特性】支持指令集融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5) |
| 74 | [I4PZE7](https://gitee.com/openharmony/device_manager/issues/I4PZE7)  | 【增强特性】支持周边不可信设备的发现    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)   |
| 75 | [I4PZE4](https://gitee.com/openharmony/device_manager/issues/I4PZE4)  | 【增强特性】支持JSAPI接口    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)  |
| 76 | [I4PZE2](https://gitee.com/openharmony/device_manager/issues/I4PZE2)  | 【新增特性】支持多用户切换    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 77 | [I4PZDZ](https://gitee.com/openharmony/device_manager/issues/I4PZDZ)  | 【增强特性】支持账号无关设备的PIN码认证    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 78 | [I4PZDY](https://gitee.com/openharmony/device_manager/issues/I4PZDY)  | 【增强特性】支持可信设备列表查询    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 79 | [I4PZDT](https://gitee.com/openharmony/device_manager/issues/I4PZDT)  | 【增强特性】支持可信设备的上下线监听    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 80 | [I4PZC9](https://gitee.com/openharmony/device_manager/issues/I4PZC9)  | 【新增特性】支持分布式设备管理接口授权控制    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116) |
| 81 | [I4PZC7](https://gitee.com/openharmony/device_manager/issues/I4PZC7)  | 【新增特性】支持设备被发现开关控制    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)   |
| 82 | [I4QESH](https://gitee.com/openharmony/device_manager/issues/I4QESH)  | 【新增特性】设备Id的查询和转换    | 分布式硬件子系统 | SIG_DistributedHardwareManagement | [@renguang1116](https://gitee.com/renguang1116)|
| 83  | [I4QEKP](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKP) | 【新增特性】基于HDF驱动框架提供light驱动能力                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 84   | [I4QEKR](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKR) | 【新增特性】Display-Layer、Display-Gralloc、Display-Gfx针对轻量系统的增强参考实现                          | 轻量系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 85   | [I4QEKV](https://gitee.com/openharmony/usb_manager/issues/I4QEKV) | 【新增特性】USB服务 HDI接口实现                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 86   | [I4OEOZ](https://gitee.com/openharmony/powermgr_power_manager/issues/I4OEOZ) | 【新增特性】监控输入亮屏输入事件，并根据输入事件进行亮、灭屏   | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 87   | [I4MBRM](https://gitee.com/openharmony/powermgr_power_manager/issues/I4MBRM) | 【新增特性】支持接近光控制锁，通话时通过接近光控制亮灭屏的特性   | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 88   | [I4MBRL](https://gitee.com/openharmony/powermgr_power_manager/issues/I4MBRL) | 【新增特性】支持显示相关的能耗调节    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 89   | [I4QGI0](https://gitee.com/openharmony/powermgr_power_manager/issues/I4QGI0) | 【新增特性】长按power Key弹出关机界面    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 90   | [I4QGJH](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4QGJH) | 【部件化专项】thermal_manager部件标准化    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 91   | [I4QGLI](https://gitee.com/openharmony/powermgr_battery_statistics/issues/I4QGLI) | 【部件化专项】battery_statistics部件标准化    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 92   | [I4QT3R](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QT3R) | 【部件化专项】全局资源调度管控子系统部件标准化 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 93   | [I4QT3Y](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT3Y) | 【新增特性】支持系统进程统一代理三方提醒 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 94   | [I4QT40](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT40) | 【新增特性】提醒后台代理计时能力 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 95   | [I4QT41](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT41) | 【新增特性】提醒代理管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 96   | [I4QT42](https://gitee.com/openharmony/notification_ans_standard/issues/I4QT42) | 【新增特性】提醒代理相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 97   | [I4QT43](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QT43) | 【新增特性】全局资源调度框架 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 98   | [I4QU0N](https://gitee.com/openharmony/resourceschedule_resource_schedule_service/issues/I4QU0N) | 【新增特性】支持Soc调频 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 99   | [I4QU0V](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0V) | 【新增特性】支持短时任务申请/注销/查询 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 100   | [I4QU0W](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0W) | 【新增特性】短时任务后台管理 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 101   | [I4QU0X](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0X) | 【新增特性】短时任务可维可测 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 102   | [I4QU0Z](https://gitee.com/openharmony/resourceschedule_background_task_mgr/issues/I4QU0Z) | 【新增特性】短时任务相关资料文档 | 全局资源调度管控子系统 | SIG_BasicSoftwareService           | [@wangwenli_wolf](https://gitee.com/wangwenli_wolf)                  |
| 103  | [I4R2M3](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2M3) | 【新增特性】时区数据部署                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 104  | [I4R2C2](https://gitee.com/openharmony/global_i18n_standard/issues/I4R2C2) | 【新增特性】多偏好语言                         | 标准系统 | SIG_ApplicationFramework          | [@mengjingzhimo](https://gitee.com/mengjingzhimo) |
| 105  | [I4R2YF](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R2YF) | 【增强特性】ResourceManager适配hap包结构和配置清单文件调整                                     | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 106  | [I4R3DO](https://gitee.com/openharmony/global_resmgr_standard/issues/I4R3DO) | 【增强特性】restool工具适配配置清单文件调整                                   | 标准系统 | SIG_ApplicationFramework          | [@jameshw](https://gitee.com/jameshw)         |
| 107  | [I4OWTZ](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4OWTZ)  | [内核子系统]【外部依赖】内核实现进程的tokenID设置     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 108  | [I4QE9K](https://gitee.com/openharmony/utils/issues/I4QE9K)  | [内核子系统]【新增特性】提供内核态驱动与用户态之间、用户态与用户态之间的内核共享能力     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 109  | [I4QM8F](https://gitee.com/openharmony/kernel_linux_build/issues/I4QM8F)  | [内核子系统]【部件化专项】Linux内核部件标准化     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 110  | [I4Q79P](https://gitee.com/openharmony/communication_ipc/issues/I4Q79P)  | [新增特性]【RPC】RPC支持跨设备收发及死亡通知 | 软总线子系统 | SIG_SoftBus | [@pilipala195](https://gitee.com/pilipala195)       |
| 111 | [I4Q79C](https://gitee.com/openharmony/communication_ipc/issues/I4Q79C)  | [新增特性]【RPC】RPC支持跨设备服务管理 | 软总线子系统 | SIG_SoftBus | [@pilipala195](https://gitee.com/pilipala195)       |
| 112 | [I4IIRC](https://gitee.com/openharmony/communication_ipc/issues/I4IIRC)  | [新增特性]【RPC】IPC实现tokenid的传递和查询 | 软总线子系统 | SIG_SoftBus | [@Xi_Yuhao](https://gitee.com/Xi_Yuhao)       |
| 113 | [I4RCE2](https://gitee.com/openharmony/security_selinux/issues/I4RCE2)  | 【部件化专项】【selinux部件】部件标准化     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 114 | [I4RCDU](https://gitee.com/openharmony/security_selinux/issues/I4RCDU)  | 【新增规格】支持只读镜像的文件的标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 115 | [I4RCDS](https://gitee.com/openharmony/security_selinux/issues/I4RCDS)  | 【新增规格】支持native进程标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 116 | [I4RCD9](https://gitee.com/openharmony/security_selinux/issues/I4RCD9)  | 【新增规格】支持SELinux虚拟文件标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 117 | [I4RCD6](https://gitee.com/openharmony/security_selinux/issues/I4RCD6)  | 【新增规格】支持SELinux文件标签设置     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 118 | [I4RCD0](https://gitee.com/openharmony/security_selinux/issues/I4RCD0)  | 【新增特性】支持SELinux策略加载和使能     | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 119 | [I4RCBT](https://gitee.com/openharmony/security_selinux/issues/I4RCBT)  | 【新增规格】提供hap应用selinux domain设置接口库    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 120 | [I4RCB7](https://gitee.com/openharmony/security_selinux/issues/I4RCB7)  | 【新增规格】提供hap应用数据目录的selinux标签设置接口库    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 121 | [I4RCAA](https://gitee.com/openharmony/security_selinux/issues/I4RCAA)  | 【新增特性】实现文件系统二级目录的selinux标签设置    | 程序访问控制子系统 | SIG_Security | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 122 | [I4JBEK](https://gitee.com/openharmony/account_os_account/issues/I4JBEK)  | [帐号子系统]支持分布式组网账号ID的派生    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 123 | [I4JBFB](https://gitee.com/openharmony/account_os_account/issues/I4JBFB)  | [账号子系统]支持分布式组网账号状态管理    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 124 | [I4IU33](https://gitee.com/openharmony/account_os_account/issues/I4IU33)  | [帐号子系统]支持本地多用户功能设置与内容修改    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 125 | [I4JBFI](https://gitee.com/openharmony/account_os_account/issues/I4JBFI)  | [账号子系统]支持本地多用户分布式信息查询    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 126 | [I4IU3V](https://gitee.com/openharmony/account_os_account/issues/I4IU3V)  | [帐号子系统]支持域账户和本地用户关联    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 127 | [I4IU6A](https://gitee.com/openharmony/account_os_account/issues/I4IU6A)  | [帐号子系统]支持本地用户约束条件配置    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 128 | [I4IU6N](https://gitee.com/openharmony/account_os_account/issues/I4IU6N)  | [帐号子系统]支持本地多用户基础信息管理    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 129 | [I4IU74](https://gitee.com/openharmony/account_os_account/issues/I4IU74)  | [帐号子系统]支持本地用户的创建和删除    | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 130 | [I4RCRT](https://gitee.com/openharmony/usb_manager/issues/I4RCRT) | [驱动子系统]SR000GNFHL：【新增特性】USB服务 部件标准化                          | 标准系统 | SIG_Driver         | [@wu-chengwen](https://gitee.com/wu-chengwen)           |
| 131 | [I4RBA4](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBA4)  | [新增特性]PKI应用签名工具支持生成签名密钥 | 安全子系统       | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)      |
| 132 | [I4RBEN](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBEN)  | [新增特性]PKI应用签名工具支持生成证书签名请求（CSR）| 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)      |
| 133 | [I4RBEA](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RBEA)  | [新增特性]PKI应用签名工具支持生成CA密钥和证书 | 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)       |
| 134 | [I4RFJP](https://gitee.com/openharmony-sig/developtools_hapsigner/issues/I4RFJP)  | [部件化专项]【PKISignCentre部件】PKISignCentre部件标准化 | 安全子系统      | SIG_Security | [@zhiwei-liu](https://gitee.com/zhiwei-liu)       |
| 135 | [I4PNX7](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4PNX7)|【分布式RDB】数据存储需求|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 136 | [I4R6T4](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4R6T4)|【部件化专项】【native_appdatamgr部件】native_appdatamgr部件标准化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 137 | [I4RFYC](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4RFYC)|【部件化专项】【objectstore部件】分布式数据对象部件标准化|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 138 | [I4H3LS](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3LS)|分布式数据对象提供JS接口|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
| 139 | [I4NUD5](https://gitee.com/openharmony/ark_js_runtime/issues/I4NUD5)|方舟C++ FFI支持继承关系|语言编译运行时子系统|SIG_CompileRuntime|[@weng-changcheng](https://gitee.com/weng-changcheng)|
| 140 | [I4P86T](https://gitee.com/openharmony/js_worker_module/issues/I4P86T)|支持Worker中可以再创建Worker，子Worker可以跟父Worker通信|语言编译运行时子系统|SIG_CompileRuntime|[@weng-changcheng](https://gitee.com/weng-changcheng)|
| 141 | [I4P7FN](https://gitee.com/openharmony/js_util_module/issues/I4P7FN)|【新增规格】container特性/LinkedList接口规格|语言编译运行时子系统|SIG_CompileRuntime|[@gongjunsong](https://gitee.com/gongjunsong)|
| 142 | [I4RG4R](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG4R)  | 【DFX】用户IAM框架DFX需求 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 143 | [I4RG4X](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG4X)  | 【user_idm】支持用户本地人脸的删除 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 144 | [I4RG55](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG55)  | 【user_idm】支持用户本地认证凭据信息查询 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 145 | [I4RG59](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG59)  | 【user_idm】支持用户本地口令的录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 146 | [I4RG5G](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5G)  | 【user_idm】支持用户本地口令的删除 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 147 | [I4RG5M](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5M)  | 【user_idm】支持用户本地人脸的录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 148 | [I4RG5R](https://gitee.com/openharmony/useriam_user_idm/issues/I4RG5R)  | 【user_idm】支持删除用户时，删除该用户的身份认证凭据 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 149 | [I4RGMX](https://gitee.com/openharmony/useriam_user_idm/issues/I4RGMX)  | 【部件化专项】【user_idm部件】部件标准化 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 150 | [I4RG8D](https://gitee.com/openharmony/useriam_user_auth/issues/I4RG8D)  | 【user_auth】支持用户本地口令认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 151 | [I4RG7W](https://gitee.com/openharmony/useriam_user_auth/issues/I4RG7W)  | 【user_auth】支持用户本地人脸认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 152 | [I4RGNO](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RGNO)  | 【部件化专项】【pin_auth部件】部件标准化 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 153 | [I4RG9E](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG9E)  | 【DFX】口令认证框架DFX需求 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 154 | [I4RG91](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG91)  | 【pin_auth】支持用户本地口令认证 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 155 | [I4RG8W](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RG8W)  | 【pin_auth】支持用户本地口令录入 | 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 156 | [I4RGU3](https://gitee.com/openharmony/useriam_pin_auth/issues/I4RGU3)  | 【pin_auth】提供软实现| 用户IAM子系统      | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 157 | [I4RCRM](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRM)|【IDE工具支持】交互事件回调耗时打印|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 158 | [I4RCRL](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRL)|【IDE工具支持】渲染流水线耗时打印|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 159 | [I4RCRK](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRK)|【DFX】ACE框架超时检测机制|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 160 | [I4RCRI](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRI)|【新增规格】卡片支持鼠标悬停事件|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 161 | [I4RCRH](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRH)|【新增特性】自定义builder|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 162 | [I4RCRG](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRG)|【新增特性】$$双向绑定编译转换支持|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)||
| 163 | [I4RCRF](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRF)|【新增特性】新增自定义组件支持访问子组件数据|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 164 | [I4RCRE](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRE)|【新增特性】新增NAPI继承机制|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 165 | [I4RCRD](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRD)|【新增规格】新增OffscreenCanvas支持抗锯齿特性|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 166 | [I4RCRC](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRC)|【新增特性】样式状态编译转换支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 167 | [I4RCRA](https://gitee.com/openharmony/ace_ace_engine/issues/I4RCRA)|【新增特性】ArkUI对接窗口新架构|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 168 | [I4Q8ZH](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZH)|【跟踪】【hiappevent部件】应用事件功能增强|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 169 | [I4RCR0](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4RCR0)|【跟踪】【hilog部件】流水日志功能增强|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 170 | [I4Q6AS](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AS)|【资料】faultloggerd部件 南北向文档需求|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 171 | [I4NJTS](https://gitee.com/openharmony/ace_engine_lite/issues/I4NJTS) |[轻量级图形子系统]支持通用touch事件、list组件支持scrollbottom/scrolltop事件|轻量系统|SIG_AppFramework|[@piggyguy](https://gitee.com/piggyguy)|
| 172 | [I4NJTD](https://gitee.com/openharmony/graphic_ui/issues/I4NJTD) |[轻量级图形子系统]list组件支持scrollbottom/scrolltop事件|轻量系统|SIG_AppFramework|[@piggyguy](https://gitee.com/piggyguy)|
| 173 | [I4RFBD](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RFBD)|【新增特性】【local_file_system】支持fat/exfat/ntfs等可插拔文件系统能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 174 | [I4RDNG](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RDNG)|【新增特性】【local_file_system】支持ext4/f2fs等用户态工具的能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 175 | [I4RE2G](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RE2G)|【新增特性】【local_file_system】支持ext4/f2fs格式镜像打包能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 176 | [I4RENG](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RENG)|【新增特性】【local_file_system】支持ext4/f2fs文件系统开机resize和fsck|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 177 | [I4RF6Z](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RF6Z)|【新增特性】【local_file_system】支持fat/exfat/ntfs等可插拔文件系统能力|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 178 | [I4RFEQ](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RFEQ)|【新增特性】【storage_service部件】支持密钥存储管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 179 | [I4RG9F](https://gitee.com/openharmony/filemanagement_storage_service/issues/I4RG9F)|【新增特性】【storage_service】CE/DE文件软加密策略管理|文件管理子系统|SIG_Kernel|[@panqinxu](https://gitee.com/panqinxu)|
| 180 | [I4PW8P](https://gitee.com/openharmony/build/issues/I4PW8P)|【新增特性】支持生成部件列表和部件依赖关系|编译构建子系统|SIG_CompileRuntime|@烈烈(https://gitee.com/xiaolielie)|
| 181   |[I4ROL2](https://gitee.com/openharmony/applications_call/issues/I4ROL2) |【通话】-通话中支持DTMF键盘 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 182   |[I4ROL0](https://gitee.com/openharmony/applications_call/issues/I4ROL0) |【通话】-通话中显示状态及计时 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 183   |[I4ROKZ](https://gitee.com/openharmony/applications_call/issues/I4ROKZ) |【通话】-来电支持通话页面拉起| 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 184   |[I4ROKY](https://gitee.com/openharmony/applications_call/issues/I4ROKY) |【通话】-通话中显示联系人号码和姓名 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 185   |[I4ROLD](https://gitee.com/openharmony/applications_call/issues/I4ROLD) |【通话】-来电显示通知 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 186   |[I4ROLG](https://gitee.com/openharmony/applications_call/issues/I4ROLG) |【短信】- 短信 - 短信接收 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 187   |[I4ROKU](https://gitee.com/openharmony/applications_mms/issues/I4ROKU) |【短信】- 短信 - 短信接收 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 188   |[I4QKLL](https://gitee.com/openharmony/applications_mms/issues/I4QKLL) |【短信】- 短信 - 短信单发（单卡）| 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|
| 189   |[I4QKLK](https://gitee.com/openharmony/applications_mms/issues/I4QKLK) |【短信】- 短信 - 短信送达报告 | 标准系统    | SIG_SystemApplication   |[@sunjunxiong](https://gitee.com/sunjunxiong)|

## OpenHarmony 3.1.3.2版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.3.2 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第2轮测试，验收:    |
| L0L1: 不涉及                                       |
| L2:                                |
| **API****变更：**：                 |
| **L0L1****转测试时间：2022-1-13**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220112_162158/version-Master_Version-OpenHarmony_3.1.3.2-20220112_162158-hispark_pegasus.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220112_162321/version-Master_Version-OpenHarmony_3.1.3.2-20220112_162321-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220112_162233/version-Master_Version-OpenHarmony_3.1.3.2-20220112_162233-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-1-13**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220112_162702/version-Master_Version-OpenHarmony_3.1.3.2-20220112_162702-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.3.2/20220112_173827/L2-SDK-MAC.tar.gz    |
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.2/20220113_215637/version-Master_Version-OpenHarmony_3.1.3.2-20220113_215637-dayu200.tar.gz |

**已解决的ISSUE单列表：**
无

**需求列表:**
| no   | issue                                                        | feture description                                           | platform       | sig                      | owner                                                 |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :------------- | :----------------------- | :---------------------------------------------------- |
| 1    | [I4QGKM](https://gitee.com/openharmony/powermgr_battery_manager/issues/I4QGKM) | 【部件化专项】battery_manage部件标准化 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 2    | [I412F4](https://gitee.com/openharmony/powermgr_power_manager/issues/I412F4) | 【省电模式】 支持省电模式                                    | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 3    | [I4GYBV](https://gitee.com/openharmony/powermgr_thermal_manager/issues/I4GYBV) | 【新增特性】提供温升监控接口                                 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |
| 4    | [I4P7FC](https://gitee.com/openharmony/third_party_musl/issues/I4P7FC) | [语言编译运行时子系统]提供NDK所需的musl版本的libc基础库以及相应头文件 | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 5    | [I4P7F9](https://gitee.com/openharmony/third_party_musl/issues/I4P7F9) | [语言编译运行时子系统]提供NDK所需的libc++库以及相应头文件    | 标准系统       | SIG_CompileRuntime       | [@huanghuijin](https://gitee.com/huanghuijin)         |
| 6    | [I4OGCN](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCN?from=project-issue) | 【增强特性】【DMS】根据指定设备发起迁移能力，接收迁移结果    | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 7    | [I4OGCM](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGCM?from=project-issue) | 【新增特性】【任务管理】提供获取实时任务接口  | 标准系统       | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)         |
| 8    | [I4PBOK](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBOK) | 【新增特性】通知支持多用户                                   | 事件通知子系统 | SIG_ApplicationFramework | [@xzz_0810](https://gitee.com/xzz_0810)               |
| 9    | [I4PCM4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM4) | 【新增特性】上下文提供应用/Hap包/组件信息查询能力            | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 10   | [I4PCM8](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM8) | 【新增特性】应用启动/组件切换流程trace                       | 元能力子系统   | SIG_ApplicationFramework | [@silent-dye](https://gitee.com/silent-dye)           |
| 11   | [I4PCNZ ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCP6) | 【新增特性】服务组件泄漏检测                                 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 12   | [I4PCPP](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPP) | 【新增特性】上下文适配多用户                                 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 13   | [I4PCPR](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPR) | 【新增特性】非并发模式下，禁止非当前用户的应用通过其他方式自启 | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 14   | [I4PCPV](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPV) | 【新增特性】提供指定用户启动组件的系统接口                   | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 15   | [I4PCQ1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQ1) | 【新增特性】提供指定用户管理应用的系统接口                   | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu](https://gitee.com/xuezhongzhu)         |
| 16   | [I4PCQJ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQJ) | 【新增特性】对外接口适配多用户                               | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 17   | [I4PCQP](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQP) | 【新增特性】支持singleuser的运行模式                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 18   | [I4PCQU](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQU) | 【新增特性】启动初始化默认用户                               | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 19   | [I4PCQW](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQW) | 【新增特性】启动用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 20   | [I4PCQY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCQY) | 【新增特性】切换用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 21   | [I4PCR2](https://gitee.com/openharmony/aafwk_standard/issues/I4PCR2) | 【新增特性】停止用户                                         | 元能力子系统   | SIG_ApplicationFramework | [@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810) |
| 22   | [I4PBP7](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBP7)|【新增特性】支持应用发送模板通知（调试能力）|事件通知子系统|SIG_ApplicationFramework|[@xzz_0810](https://gitee.com/xzz_0810)|
| 23   | [I4PBPE](https://gitee.com/openharmony/notification_ans_standard/issues/I4PBPE)|【新增特性】支持进度条通知|事件通知子系统|SIG_ApplicationFramework|[@xzz_0810](https://gitee.com/xzz_0810)|
| 24   | [I4PCGY](https://gitee.com/openharmony/aafwk_standard/issues/I4PCGY)|【增强特性】新增卡片开发基类|元能力子系统|SIG_ApplicationFramework|[@lsq1474521181](https://gitee.com/lsq1474521181)|
| 25   | [I4PCH9](https://gitee.com/openharmony/aafwk_standard/issues/I4PCH9)|【增强特性】通过配置文件配置服务卡片|元能力子系统|SIG_ApplicationFramework|[@lsq1474521181](https://gitee.com/lsq1474521181)|
| 26   | [I4PCLL](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLL)|【新增特性】JS提供的应用级别上下文|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 27   | [I4PCLN](https://gitee.com/openharmony/aafwk_standard/issues/I4PCLN)|【新增特性】Abilty的状态恢复|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 28   | [I4PCM6](https://gitee.com/openharmony/aafwk_standard/issues/I4PCM6)|【新增特性】提供应用或组件状态监听/查询能力|元能力子系统|SIG_ApplicationFramework|[@silent-dye](https://gitee.com/silent-dye)|
| 29   | [I4PCP1](https://gitee.com/openharmony/aafwk_standard/issues/I4PCP1)|【新增特性】应用运行信息查询|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 30   | [I4PCPG](https://gitee.com/openharmony/aafwk_standard/issues/I4PCPG)|【增强特性】支持系统环境变化通知|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu](https://gitee.com/xuezhongzhu)|
| 31   | [I4PCR8](https://gitee.com/openharmony/aafwk_standard/issues/I4PCR8)|【增强特性】支持常驻进程开机启动|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 32   | [I4PCSB](https://gitee.com/openharmony/aafwk_standard/issues/I4PCSB)|【新增特性】强制停止进程|元能力子系统|SIG_ApplicationFramework|[@xuezhongzhu0810](https://gitee.com/xuezhongzhu0810)|
| 33   | [I4PCV4](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV4)|【新增特性】支持任务切换|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 34   | [I4PCV9](https://gitee.com/openharmony/aafwk_standard/issues/I4PCV9)|【新增特性】多任务管理|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 35   | [I4PCVA](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVA)|【新增特性】支持任务锁|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 36   | [I4PCVF](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVF)|【新增特性】支持任务清除|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 37   | [I4PCVZ](https://gitee.com/openharmony/aafwk_standard/issues/I4PCVZ)|【新增特性】支持指定displayId启动Ability|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 38   | [I4PCW3](https://gitee.com/openharmony/aafwk_standard/issues/I4PCW3)|【增强特性】pendingwant机制支持跨设备启动通用组件|元能力子系统|SIG_ApplicationFramework|[@sheilei](https://gitee.com/sheilei)|
| 39  | [I4PKYL](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYL) | 【新增特性】支持查询指定用户下的应用信息                     | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 40  | [I4PKYM](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYM) | 【新增特性】支持多用户创建                                   | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 41  | [I4PKYN](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYN) | 【新增特性】支持多用户删除                                   | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 42  | [I4PKYO](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYO) | 【新增特性】支持安装应用到指定用户                           | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 43  | [I4PKYP](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PKYP) | 【新增特性】支持卸载指定用户下的应用                         | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 44  | [I4PKY8](https://gitee.com/openharmony/appexecfwk_standard/issues/I4PO7Z) | 【新增特性】installd 提供应用空间统计、cache统计             | 包管理子系统   | SIG_ApplicationFramework | [@shuaytao](https://gitee.com/shuaytao)               |
| 45  | [I4PC12](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4PC12) | 支持基本文件异步操作API需求                                  | 标准系统       | SIG_Kernel               | [@panqinxu](https://gitee.com/panqinxu)               |
| 46  | [I4PXMP](https://gitee.com/openharmony/build/issues/I4PXMP)  | [编译构建子系统]【增强特性】统一编译入口，轻量级和标准系统使用hb可编译 | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 47  | [I4PXNS](https://gitee.com/openharmony/build/issues/I4PXNS)  | [编译构建子系统]【新增特性】提供NDK的编译模板               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 48  | [I4PXNZ](https://gitee.com/openharmony/build/issues/I4PXNZ)  | [编译构建子系统]【新增特性】 Native-SDK中提供cmake toolchain文件               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 49  | [I4PXRD](https://gitee.com/openharmony/build/issues/I4PXRD)  | [编译构建子系统]【新增特性】归一的部件定义和编译               | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)       |
| 50  | [I4JBFF](https://gitee.com/openharmony/account_os_account/issues/I4JBFF)  | [账号子系统]【新增特性】支持本地多用户信息查询               | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 51  | [I4IU2T](https://gitee.com/openharmony/account_os_account/issues/I4IU2T)  | [账号子系统]【新增特性】支持本地多用户订阅及取消订阅          | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 52  | [I4IU3B](https://gitee.com/openharmony/account_os_account/issues/I4IU3B)  | [账号子系统]【新增特性】支持本地多用户启动、停止、切换动作     | 账号子系统 | SIG_BscSoftSrv | [@jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)       |
| 53  | [I4Q9QZ](https://gitee.com/openharmony/kernel_liteos_m/issues/I4Q9QZ)  | [内核子系统]【增强特性】支持南向接口融合    | 内核子系统 | SIG_Kernel | [@leonchan5](https://gitee.com/leonchan5)       |
| 54  | [I4Q8F8](https://gitee.com/openharmony-sig/useriam_faceauth/issues/I4Q8F8)  | [用户IAM子系统]【新增特性】提供L2人脸识别框架     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 55  | [I4Q8F5](https://gitee.com/openharmony-sig/useriam_faceauth/issues/I4Q8F5)  | [用户IAM子系统]【新增特性】提供L2人脸录入控件     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 56  | [I4Q8F3](https://gitee.com/openharmony-sig/useriam_faceauth/issues/I4Q8F3)  | [用户IAM子系统]【新增特性】提供L2统一身份认证JS接口     | 用户IAM子系统 | SIG_Security | [@wangxu](https://gitee.com/wangxu43)       |
| 57  | [I4LKQ0](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4LKQ0)  | [内核子系统]【新增特性】cpuset与cpu热插拔解耦     | 内核子系统 | SIG_Kernel | [@liuyoufang](https://gitee.com/liuyoufang)       |
| 58  | [I4HAMI](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMI)  | 【data_share_ability】支持跨应用订阅数据库的变化    | 标准系统 | SIG_DataManagement | [@verystone](https://gitee.com/verystone)      |
| 59   | [I4Q6AS](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AS)|【新增特性】FreezeDetector|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 60   | [I4Q6AV](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AV)|【新增特性】在Openharmony上hiview插件管理平台代理加载特性|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 61   | [I4Q6AT](https://gitee.com/openharmony/hiviewdfx_hiview/issues/I4Q6AT)|【新增特性】在Openharmony上FaultLogger添加js api|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 62   | [I4Q6B3](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B3)|【新增特性】支持内核日志的读取和落盘|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 63   | [I4Q6B1](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I4Q6B1)|【增强特性】将hilogd NDK库加入到OpenHarmony NDK包|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 64   | [I4Q8ZL](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZL)|【增强特性】日志管理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 65   | [I4Q8ZK](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZK)|【增强特性】进程异常信号处理|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 66   | [I4Q8ZJ](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZJ)|【增强特性】抓取调用栈工具|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 67   | [I4Q8ZI](https://gitee.com/openharmony/hiviewdfx_faultloggerd/issues/I4Q8ZI)|【新增特性】抓取调用栈基础库|标准系统|SIG_BasicSoftwareService|[@guochuanqi](https://gitee.com/guochuanqi)|
| 68    | [I4QEKH](https://gitee.com/openharmony/drivers_framework/issues/I4QEKH) | 【新增特性】提供共享内存相关HDI能力                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 69    | [I4QEKI](https://gitee.com/openharmony/drivers_framework/issues/I4QEKI) | 【新增特性】驱动开发工具支持标准系统驱动开发                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 70    | [I4QEKJ](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKJ) |【新增特性】HDI接口适配linux-input驱动                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 71    | [I4QEKM](https://gitee.com/openharmony/drivers_peripheral/issues/I4QEKM) | 【新增特性】提供power HDI接口能力                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 72    | [I4QEKK](https://gitee.com/openharmony/drivers_framework/issues/I4QEKK) | 【新增特性】基于HDF驱动框架提供硬件TIMER驱动                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 73    | [I4QEKL](https://gitee.com/openharmony/drivers_framework/issues/I4QEKL) | 【新增特性】基于HDF驱动框架构建统一的平台驱动对象模型                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 74    | [I4QEKN](https://gitee.com/openharmony/usb_manager/issues/I4QEKN) | 【新增特性】USB Device功能实现                           | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 75    | [I4QEKO](https://gitee.com/openharmony/usb_manager/issues/I4QEKO) | 【新增特性】USB Host功能实现                          | 标准系统 | SIG_Driver         | [@xie0812](https://gitee.com/xie0812)           |
| 76   | [I4QEPQ](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4QEPQ)|【资料】【RDB】支持QuerySql返回结果集，进一步支持更广泛的查询方式|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 77   | [I4NZP6](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4NZP6)|【RDB】增加多表查询能力|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 78   | [I4FZ6B](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4FZ6B)|【RDB】提供事务能力|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 79   | [I4HAMI](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4HAMI)|【data_share_ability】支持跨应用订阅数据库的变化|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
| 80   | [I4QC4U](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4U)|【新增特性】PC预览资源管理特性对接全球化规格|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 81   | [I4QC4S](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4S)|【新增规格】文本计时器组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 82   | [I4QC4R](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4R)|【新增规格】进度条组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 83   | [I4QC4P](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4P)|【新增规格】文字时钟组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 84   | [I4QC4N](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4N)|【新增规格】TextInput组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 85   | [I4QC4O](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4O)|【新增规格】Select组件支持|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 86   | [I4QC4K](https://gitee.com/openharmony/ace_ace_engine/issues/I4QC4K)|【新增规格】TextArea组件能力增强|ArkUI框架子系统|SIG_ArkUI|[@qieqiewl](https://gitee.com/qieqiewl)|
| 87    | [I4QGHF](https://gitee.com/openharmony/powermgr_power_manager/issues/I4QGHF) | 【部件化专项】power_manage部件标准化 | 标准系统       | SIG_HardwareMgr          | [@aqxyjay](https://gitee.com/aqxyjay)                 |


## OpenHarmony 3.1.3.1版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.3.1 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代四第1轮测试，验收:   |
| L0L1: 不涉及                                      |
| L2:                               |
| **API****变更：**：                |
| **L0L1****转测试时间：2022-1-6**                          |
| **L0L1****转测试版本获取路径：**                            |
| hispark_pegasus版本:   http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_092223/version-Master_Version-OpenHarmony_3.1.3.1-20220106_092223-hispark_pegasus.tar.gz |
| hispark_taurus版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_092343/version-Master_Version-OpenHarmony_3.1.3.1-20220106_092343-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_092109/version-Master_Version-OpenHarmony_3.1.3.1-20220106_092109-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2022-1-6**                            |
| **L2****转测试版本获取路径：**                              |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_205029/version-Master_Version-OpenHarmony_3.1.3.1-20220106_205029-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac：https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.3.1/20220106_235728/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_160053/version-Master_Version-OpenHarmony_3.1.3.1-20220106_160053-hispark_taurus_L2.tar.gz |
| RK3568版本: http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.3.1/20220106_160117/version-Master_Version-OpenHarmony_3.1.3.1-20220106_160117-dayu200.tar.gz |

**已解决的ISSUE单列表：**
无

**需求列表:**
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I410YD](https://gitee.com/openharmony/powermgr_battery_manager/issues/I410YD) | 【充放电&Battery服务】 支持关机充电特性                     | 标准系统      | SIG_HardwareMgr         | [@aqxyjay](https://gitee.com/aqxyjay)                |
| 2   | [I410Y1](https://gitee.com/openharmony/powermgr_battery_manager/issues/I410Y1) | 【充放电&Battery服务】 电池温度异常关机保护                 | 标准系统      | SIG_HardwareMgr         | [@aqxyjay](https://gitee.com/aqxyjay)                |
| 3   | [I4OGD2](https://gitee.com/openharmony/distributedschedule_dms_fwk/issues/I4OGD2?from=project-issue) | 【资料】跨设备组件调用新增/增强特性资料说明                 | 标准系统      | SIG_BasicSoftwareService | [@cangegegege](https://gitee.com/cangegegege)        |
| 4   | [I4PDB8](https://gitee.com/openharmony/drivers_framework/issues/I4PDB8) | 【新增特性】提供设备PnP事件监听接口                         | 标准系统      | SIG_HardwareMgr         | [@yuanbogit](https://gitee.com/yuanbogit)            |
| 5   | [I4PDAV](https://gitee.com/openharmony/applications_contacts/issues/I4PDAV) | 【联系人】通话记录 - 基本内容（名字、号码、时间、来电次数等） | 标准系统      | SIG_SystemApplication   | [@lv-zhongwei](https://gitee.com/lv-zhongwei)        |
| 6   | [I4PDAW](https://gitee.com/openharmony/applications_contacts/issues/I4PDAW) | 【联系人】通话记录 - 通话记录列表显示（TAB/列表展示）       | 标准系统      | SIG_SystemApplication   | [@lv-zhongwei](https://gitee.com/lv-zhongwei)        |
| 7    | [I4PDAY](https://gitee.com/openharmony/applications_systemui/issues/I4PDAY) | 【SystemUI】【状态栏】提示胶囊                               | 标准系统       | SIG_SystemApplication    | [@lv-zhongwei](https://gitee.com/lv-zhongwei)         |
| 8   | [I4PCX8](https://gitee.com/openharmony/communication_ipc/issues/I4PCX8?from=project-issue) | 【RPC】进程间IPC、设备间RPC支持HiTrace能力                  | 标准系统      | SIG_SoftBus             | [@Xi_Yuhao](https://gitee.com/Xi_Yuhao)              |
| 9  | [I4PD3K](https://gitee.com/openharmony/startup_init_lite/issues/I4PD3K) | 进程退出后的回收处理策略配置能力增强                        | 标准系统      | SIG_BscSoftSrv          | [@xionglei16](https://gitee.com/xionglei16)          |
| 10  | [I4PD3C](https://gitee.com/openharmony/startup_init_lite/issues/I4PD3C) | 支持SA类进程按需启动                                        | 标准系统      | SIG_BscSoftSrv          | [@xionglei16](https://gitee.com/xionglei16)          |
| 11  | [I4NZVP](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4NZVP) | 【distributed_kv_store】提供分布式数据库JS API              | 标准系统      | SIG_DataManagement      | [@widecode](https://gitee.com/widecode)              |

## OpenHarmony 3.1.2.5(Beta)版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.2.5(Beta) *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第4轮测试，验收:    |
| L0L1: 不涉及                                       |
| L2: IT1~TI3全量需求验证                                     |
| **API****变更：**：本次转测特性不涉及API变更                 |
| **L0L1****转测试时间：2021-12-30**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211230_193318/version-Master_Version-OpenHarmony_3.1.2.5-20211230_193318-hispark_pegasus_OpenHarmony-3.1-Beta.tar.gz |
| hispark_taurus版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211230_193358/version-Master_Version-OpenHarmony_3.1.2.5-20211230_193358-hispark_taurus_LiteOS_OpenHarmony-3.1-Beta.tar.gz |
| hispark_taurus_linux版本:  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211230_193339/version-Master_Version-OpenHarmony_3.1.2.5-20211230_193339-hispark_taurus_Linux_OpenHarmony-3.1-Beta.tar.gz |
| **L2****转测试时间：2021-12-30**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows： http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1_Beta/20220106_185129/version-Master_Version-OpenHarmony_3.1_Beta-20220106_185129-ohos-sdk_OpenHarmony-3.1-Beta.tar.gz |
| hi3516dv300-L2版本 SDK mac： https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1_Beta/20220106_224324/L2-SDK-MAC.tar.gz    |
| hi3516dv300-L2版本：  http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211231_191018/version-Master_Version-OpenHarmony_3.1.2.5-20211231_191018-hispark_taurus_L2_OpenHarmony-3.1-Beta.tar.gz |
| RK3568版本:http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.5/20211231_191442/version-Master_Version-OpenHarmony_3.1.2.5-20211231_191442-dayu200_OpenHarmony-3.1-Beta.tar.gz |

**2021/12/29已解决的ISSUE单列表：**
|serialNo| ISSUE                                                        | 问题描述                                                     |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|1||IDE配套问题，修改js版本号，否则IDE工具无法下载到SDK包，需要修改为1.0.6版本| 
|2||二级界面无法跳转，点击时间设置、亮度调节、音量调节等不生效问题| 
|3||状态栏依旧有Home/Back/功能键显示，但这些键点击无效| 
|4|I4O67X|标准系统_3516执行xts测试套ActsAnsDoNotDisturbTest报ace异常调用栈问题，影响社区流水线稳定性测试| 
|5|I4OBCG|libnotification.z.so文件NO Rpath/RunPath选项有问题| 
|6|I4ND5U|【openharmony】kr3568单板manager用例7失败| 

**2021/12/28已解决的ISSUE单列表：**
|serialNo| ISSUE                                                        | 问题描述                                                     |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
|1|I4NEUX|执行xts测试套ActsBmsGetInfosTest报ace异常调用栈问题，影响社区流水线测试|
|2|I4O311|/system/bin/display-hotplug.sh脚本编译到正式版本中，疑似后门，应删除| 
|3||IDE配套问题，低代码工程适配失败| 
|4||EGG   心跳次数是三位数时，会文字显示不完整| 
|5||计算器 带小数乘除计算会有误差1.1*6=6.6000000000000006| 
|6||3516的性能不行导致绑定超时失败，client失败| 



**需求列表:**
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
|1|[I4M8FW](https://gitee.com/openharmony/account_os_account/issues/I4M8FF?from=project-issue)|【新增特性】支持应用账号基础信息管理|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|2|[I4MBQE](https://gitee.com/openharmony/miscservices_time/issues/I4MBQE)|新增特性：时间时区管理|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|3|[I4MBQF](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBQF)|【增强特性】【事件通知子系统】线程间EventHandler支持HiTrace能力|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|4|[I4MBQG](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQG?from=project-issue)|【资料】hisysevent部件资料需求|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|5|[I4MBQH](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQH?from=project-issue)|【新增特性】支持鸿蒙HiSysEvent部件提供查询接口|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|6|[I4MBQI](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQI?from=project-issue)|【新增特性】提供工具查询或者订阅系统事件|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|7|[I4MBQJ](https://gitee.com/openharmony/hiviewdfx_hisysevent/issues/I4MBQJ?from=project-issue)|【新增特性】支持鸿蒙HiSysEvent部件提供订阅接口|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|8|[I4MBQK](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I4MBQK?from=project-issue)|【资料】hiappevent部件资料需求|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|9|[I4MBQL](https://gitee.com/openharmony/hiviewdfx_hiappevent/issues/I4MBQL?from=project-issue)|【新增特性】支持鸿蒙hiappevent部件的C接口|标准系统|SIG_BasicSoftwareService|[@yaomanhai](https://gitee.com/yaomanhai)|
|10|[I4MBQM](https://gitee.com/openharmony/build/issues/I4MBQM?from=project-issue)|【新增规格】L0-L2支持统一的部件配置|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|11|[I4MBQN](https://gitee.com/openharmony/build/issues/I4MBQN?from=project-issue)|【新增规格】L0-L2支持使用统一的编译命令进行编译|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|12|[I4MBQO](https://gitee.com/openharmony/build/issues/I4MBQO?from=project-issue)|【新增规格】L0-L2支持使用统一的编译流程|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|13|[I4MBQP](https://gitee.com/openharmony/build/issues/I4MBQP?from=project-issue)|【新增规格】L0-L2支持使用统一的gn模板|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|14|[I4MBQQ](https://gitee.com/openharmony/build/issues/I4MBQQ?from=project-issue)|【新增规格】L0-L2支持统一的产品配置|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|15|[I4MBQR](https://gitee.com/openharmony/build/issues/I4MBQR?from=project-issue)|【资料】制定gn编码规范和最佳实践指导|标准系统|SIG_CompileRuntime|[@wangxing-hw](https://gitee.com/wangxing-hw)|
|16|[I4MBQS](https://gitee.com/openharmony/account_os_account/issues/I4MBQS?from=project-issue)|【新增特性】支持应用账号信息查询|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|17|[I4MBQT](https://gitee.com/openharmony/account_os_account/issues/I4MBQT?from=project-issue)|【新增特性】支持应用账号功能设置与内容修改|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|18|[I4MBQU](https://gitee.com/openharmony/account_os_account/issues/I4MBQU?from=project-issue)|【新增特性】支持应用账号订阅及取消订阅|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|19|[I4MBQV](https://gitee.com/openharmony/account_os_account/issues/I4MBQV?from=project-issue)|【DFX】应用账号基础信息约束|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|20|[I4MBQW](https://gitee.com/openharmony/account_os_account/issues/I4MBQW?from=project-issue)|【新增特性】支持应用账号的新增和删除|标准系统|SIG_BasicSoftwareService|[@verystone](https://gitee.com/verystone)|
|21|[I4MBQX](https://gitee.com/openharmony/global_cust_lite/issues/I4MBQX?from=project-issue)|【增强特性】定制框架基础能力|标准系统|SIG_ApplicationFramework|[@zhengbin5](https://gitee.com/zhengbin5)|
|22|[I4MBQY](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBQY?from=project-issue)|【新增特性】资源编译工具支持增量编译|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|23|[I4MBQZ](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBQZ?from=project-issue)|【增强特性】时间段格式化|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|24|[I4MBR0](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR0?from=project-issue)|【增强特性】区域表示和属性|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|25|[I4MBR1](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR1?from=project-issue)|【增强特性】单复数支持|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|26|[I4MBR2](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR2?from=project-issue)|【增强特性】字符串排序|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|27|[I4MBR3](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR3?from=project-issue)|【增强特性】电话号码处理|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|28|[I4MBR4](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR4?from=project-issue)|【新增特性】字母表检索|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|29|[I4MBR5](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR5?from=project-issue)|【新增特性】度量衡体系和格式化|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|30|[I4MBR7](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR7?from=project-issue)|【新增特性】日历&本地历法|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|31|[I4MBR8](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR8?from=project-issue)|【增强特性】unicode字符属性|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|32|[I4MBR9](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBR9?from=project-issue)|【增强特性】断词断行|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|33|[I4MBRA](https://gitee.com/openharmony/global_resmgr_standard/issues/I4MBRA?from=project-issue)|【新增特性】系统资源管理|标准系统|SIG_ApplicationFramework|[@jameshw](https://gitee.com/jameshw)|
|34|[I4MBRB](https://gitee.com/openharmony/global_i18n_standard/issues/I4MBRB?from=project-issue)|【新增特性】rawfile资源管理|标准系统|SIG_ApplicationFramework|[@mengjingzhimo](https://gitee.com/mengjingzhimo)|
|35|[I4MBRC](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRC?from=project-issue)|【hiperf部件】采样数据展示|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|36|[I4MBRD](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRD?from=project-issue)|【hiperf部件】性能数据采样记录|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|37|[I4MBRE](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRE?from=project-issue)|【hiperf部件】性能数据计数统计|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|38|[I4MBRF](https://gitee.com/openharmony/communication_wifi/issues/I4MBRF?from=project-issue)|【新增特性】支持STA基础特性的JS API接口|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|39|[I4MBRG](https://gitee.com/openharmony/communication_wifi/issues/I4MBRG?from=project-issue)|【新增特性】支持STA基础特性JS API资料文档|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|40|[I4MBRH](https://gitee.com/openharmony/communication_wifi/issues/I4MBRH?from=project-issue)|【新增特性】支持STA基础特性|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|41|[I4MBRI](https://gitee.com/openharmony/communication_wifi/issues/I4MBRI?from=project-issue)|【新增特性】支持SoftAP基础特性|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|42|[I4MBRJ](https://gitee.com/openharmony/communication_wifi/issues/I4MBRJ?from=project-issue)|【新增特性】提供WiFi模块的维测能力|标准系统|SIG_SoftBus|[@cheng_guohong](https://gitee.com/cheng_guohong)|
|43|[I4MBRK](https://gitee.com/openharmony/usb_manager/issues/I4MBRK?from=project-issue)|【新增特性】USB服务JS接口实现|标准系统|SIG_DistributedHardwareManagement|[@hhh2](https://gitee.com/hhh2)|
|44|[I4MBRP](https://gitee.com/openharmony/sensors_sensor/issues/I4MBRP?from=project-issue)|【泛Sensor】地磁场偏角和倾角|标准系统|SIG_DistributedHardwareManagement|[@hhh2](https://gitee.com/hhh2)|
|45|[I4MBRQ](https://gitee.com/openharmony/sensors_sensor/issues/I4MBRQ?from=project-issue)|【泛Sensor】地磁场偏角和倾角|标准系统|SIG_DistributedHardwareManagement|[@hhh2](https://gitee.com/hhh2)|
|45|[I4MBRR](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4MBRR?from=project-issue)|【资料】distributed_kv_store分布式数据库支持按谓词查询条件进行数据库记录的跨设备同步和订阅|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
|46|[I4MBRS](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4MBRS?from=project-issue)|【distributed_kv_store】分布式数据库支持按谓词查询条件进行数据库记录的跨设备同步和订阅|标准系统|SIG_DataManagement|[@widecode](https://gitee.com/widecode)|
|47|[I4MBRT](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4MBRT?from=project-issue)|【资料】RDB提供数据库级安全加密|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|48|[I4MBRU](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I4MBRU?from=project-issue)|【RDB】支持数据库加密|标准系统|SIG_DataManagement|[@mangtsang](https://gitee.com/mangtsang)|
|49|[I4MBRV](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRV?from=project-issue)|【samgr】系统服务状态监控|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|50|[I4MBRW](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRW?from=project-issue)|【samgr】服务进程内的SA名单管控|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|51|[I4MBRX](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRX?from=project-issue)|【samgr】加载指定系统服务|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|52|[I4MBRY](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRY?from=project-issue)|【samgr】系统服务进程管理|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|53|[I4MBRZ](https://gitee.com/openharmony/distributedschedule_samgr/issues/I4MBRZ?from=project-issue)|【samgr】全量服务列表初始化|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|54|[I4MBS0](https://gitee.com/openharmony/communication_dsoftbus/issues/I4MBS0?from=project-issue)|【新增特性】【组网】软总线支持网络切换组网|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|55|[I4MBS1](https://gitee.com/openharmony/communication_dsoftbus/issues/I4MBS1?from=project-issue)|【新增特性】【传输】软总线提供传输ExtAPI接口|标准系统|SIG_SoftBus|[@bigpumpkin](https://gitee.com/bigpumpkin)|
|56|[I4MBS2](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4MBS2?from=project-issue)|【新增特性】支持statfs API能力需求|标准系统|SIG_DataManagement|[@zhangzhiwi](https://gitee.com/zhangzhiwi)|
|57|[I4MBS3](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS3?from=project-issue)|【新增特性】支持长时任务通知|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|58|[I4MBS4](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS4?from=project-issue)|【新增特性】通知系统API支持权限管理|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|59|[I4MBS5](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS5?from=project-issue)|【新增特性】支持设置通知振动|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|60|[I4MBS6](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS6?from=project-issue)|【新增特性】支持通知声音设置和查询|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|61|[I4MBS7](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS7?from=project-issue)|【新增特性】通知支持免打扰|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|62|[I4MBS8](https://gitee.com/openharmony/notification_ans_standard/issues/I4MBS8?from=project-issue)|【新增特性】支持会话类通知|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|63|[I4MBS9](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBS9?from=project-issue)|【新增特性】EventHandler支持hitrace|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|64|[I4MBSA](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBSA?from=project-issue)|【新增特性】支持系统公共事件管理特性|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|65|[I4MBSB](https://gitee.com/openharmony/notification_ces_standard/issues/I4MBSB?from=project-issue)|【新增特性】支持eventEmitter|标准系统|SIG_BasicSoftwareService|[@autumn330](https://gitee.com/autumn330)|
|66|[I4MBSC](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSC?from=project-issue)|【增强特性】支持Module和Ability的srcPath字段|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|67|[I4MBSD](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSD?from=project-issue)|【新增特性】支持多hap包安装|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|68|[I4MBSE](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSE?from=project-issue)|【新增特性】提供桌面包管理客户端|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|69|[I4MBSF](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSF?from=project-issue)|【新增特性】提供清除缓存数据js api|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|70|[I4MBSG](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSG?from=project-issue)|【增强特性】安装包信息查询|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|71|[I4MBSH](https://gitee.com/openharmony/appexecfwk_standard/issues/I4MBSH?from=project-issue)|【新增特性】多hap安装时的签名校验|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|72|[I4MBSI](https://gitee.com/openharmony/aafwk_standard/issues/I4MBSI?from=project-issue)|【新增特性】ZIDL工具自动生成Extension C++服务端及客户端接口文件|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|73|[I4MBT4](https://gitee.com/openharmony/aafwk_standard/issues/I4MBT4?from=project-issue)|【增强特性】支持常驻进程开机启动|标准系统|SIG_ApplicationFramework|[@gwang2008](https://gitee.com/gwang2008)|
|74|[I4MBTN](https://gitee.com/openharmony/kernel_linux_5.10/issues/I4MBTN?from=project-issue)|【新增特性】支持CMA复用特性|标准系统|SIG_Kernel|[@liuyoufang](https://gitee.com/liuyoufang)|
|75|[I4MBTO](https://gitee.com/openharmony/third_party_musl/issues/I4MBTO?from=project-issue)|【新增特性】支持内存占用分类查询|标准系统|SIG_CompileRuntime|[@huanghuijin](https://gitee.com/huanghuijin)|
|76|[I4MBTP](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTP?from=project-issue)|【增强特性】传感器驱动模型能力增强|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|77|[I4MBTQ](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTQ?from=project-issue)|【增强特性】传感器器件驱动能力增强|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|78|[I4MBTR](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTR?from=project-issue)|【新增特性】Display-Layer HDI接口针对L2的参考实现；  Display-Gralloc HDI接口针对L2的参考实现；  Display-Device  HDI接口针对L2的参考实现；|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|79|[I4MBTS](https://gitee.com/openharmony/drivers_framework/issues/I4MBTS?from=project-issue)|【增强特性】 HDF-Input设备能力丰富|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|80|[I4MBTT](https://gitee.com/openharmony/drivers_framework/issues/I4MBTT?from=project-issue)|【新增特性】支持Linux/Liteos-a内核系统级休眠唤醒|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|81|[I4MBTU](https://gitee.com/openharmony/drivers_framework/issues/I4MBTU?from=project-issue)|【新增特性】支持同步/异步电源管理调用|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|82|[I4MBTV](https://gitee.com/openharmony/drivers_framework/issues/I4MBTV?from=project-issue)|【新增特性】提供hcs宏式解析接口|标准系统|SIG_DriverFramework|[@zianed](https://gitee.com/zianed)|
|83|[I4MBU1](https://gitee.com/openharmony/applications_settings/issues/I4MBU1?from=project-issue)|【设置公共数据存储】Settings数据管理API|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|84|[I4MBU3](https://gitee.com/openharmony/applications_settings/issues/I4MBU3?from=project-issue)|【设置】系统-时间设置|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|85|[I4MBU5](https://gitee.com/openharmony/applications_settings/issues/I4MBU5?from=project-issue)|【设置】声音管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|86|[I4MBU6](https://gitee.com/openharmony/applications_settings/issues/I4MBU6?from=project-issue)|【设置】基础能力-数据管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|87|[I4MBU7](https://gitee.com/openharmony/applications_settings/issues/I4MBU7?from=project-issue)|【设置】基础能力-默认值管理|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|88|[I4MBU8](https://gitee.com/openharmony/applications_settings/issues/I4MBU8?from=project-issue)|【设置】基础能力-多设备形态差异化构建|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|89|[I4MBU9](https://gitee.com/openharmony/applications_systemui/issues/I4MBU9?from=project-issue)|【SystemUI】【通知】通知组件化|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|90|[I4MBUB](https://gitee.com/openharmony/ark_ts2abc/issues/I4MBUB?from=project-issue)|【新增特性】提供windows/MacOS/Linux的前端编译工具链|标准系统|SIG_CompileRuntime|[@godmiaozi](https://gitee.com/godmiaozi)|
|91|[I4MBUC](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUC?from=project-issue)|【新增特性】Openharmony jsapi替换为ark版本|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|92|[I4MBUD](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUD?from=project-issue)|【新增规格】内存管理分配回收功能/Concurrent mark算法以及Concurrent Sweep实现|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|93|[I4MBUE](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUE?from=project-issue)|【新增特性】OpenharmonyOS上默认内置应用替换为ark版本|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|94|[I4MBUF](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUF?from=project-issue)|【增强特性】Inline Cache功能|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|95|[I4MBUG](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUG?from=project-issue)|【增强特性】支持解释器call指令优化|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|96|[I4MBUH](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUH?from=project-issue)|【新增规格】CPU Profiler运行时实现|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|97|[I4MBUI](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUI?from=project-issue)|【新增规格】内存管理分配回收功能/ 支持Old Space的Partial GC|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|98|[I4MBUJ](https://gitee.com/openharmony/ark_ts2abc/issues/I4MBUJ?from=project-issue)|【新增特性】OpenHarmony应用工程编译构建能力  【描述】  1、在正常构建场景下，能够将开发者程序代码编译成方舟字节码  2、在编译出现错误时，输出准确编译错误提示信息|标准系统|SIG_CompileRuntime|[@godmiaozi](https://gitee.com/godmiaozi)|
|99|[I4MBUK](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUK?from=project-issue)|【新增规格】JS运行时支持预览器|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|100|[I4MBUL](https://gitee.com/openharmony/ark_js_runtime/issues/I4MBUL?from=project-issue)|【新增规格】方舟支持调试并且支持attach模式|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|101|[I4MBUM](https://gitee.com/openharmony/js_util_module/issues/I4MBUM?from=project-issue)|【新增规格】提供libc，c++，clang基础测试框架|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|102|[I4MBUN](https://gitee.com/openharmony/js_sys_module/issues/I4MBUN?from=project-issue)|【新增规格】支持utils特性  /提供process接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|103|[I4MBUO](https://gitee.com/openharmony/js_util_module/issues/I4MBUO?from=project-issue)|【新增规格】支持utils特性  /提供提供Scope接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|104|[I4MBUP](https://gitee.com/openharmony/js_util_module/issues/I4MBUP?from=project-issue)|【新增规格】支持utils特性  /提供提供Base64接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|105|[I4MBUQ](https://gitee.com/openharmony/js_util_module/issues/I4MBUQ?from=project-issue)|【新增规格】支持utils特性  /提供提供RationalNumber接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|106|[I4MBUR](https://gitee.com/openharmony/js_util_module/issues/I4MBUR?from=project-issue)|【新增规格】支持语言增强特性/提供JS Typeof 接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|107|[I4MBUS](https://gitee.com/openharmony/js_api_module/issues/I4MBUS?from=project-issue)|【新增规格】支持URI特性 /提供 URI解析接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|108|[I4MBUT](https://gitee.com/openharmony/js_util_module/issues/I4MBUT?from=project-issue)|【新增规格】支持utils特性提供LRUBuffer接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|109|[I4MBUU](https://gitee.com/openharmony/js_api_module/issues/I4MBUU?from=project-issue)|【新增规格】支持XML特性/提供XmlPullParser 接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|110|[I4MBUV](https://gitee.com/openharmony/js_api_module/issues/I4MBUV?from=project-issue)|【新增规格】支持XML特性/提供xmlSerializer 接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|111|[I4MBUW](https://gitee.com/openharmony/js_api_module/issues/I4MBUW?from=project-issue)|【新增规格】支持XML特性/提供xml2JSObject 接口规格|标准系统|SIG_CompileRuntime|[@wuzhefengh](https://gitee.com/wuzhefengh)|
|112|[I4MBUX](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUX?from=project-issue)|【新增规格】资源管理特性对接全球化规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|113|[I4MBUY](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUY?from=project-issue)|【新增规格】事件中增加Target获取尺寸|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|114|[I4MBUZ](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBUZ?from=project-issue)|【新增规格】Swiper组件支持设置缓存cache|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|115|[I4MBV1](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV1?from=project-issue)|【新增规格】Image组件支持同步、异步渲染设置|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|116|[I4MBV3](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV3?from=project-issue)|【新增规格】样式设置特性增加组件多态样式设置规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|117|[I4MBV5](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV5?from=project-issue)|【新增规格】字母索引条组件增加提示菜单内容扩展规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|118|[I4MBV6](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV6?from=project-issue)|【新增规格】组件自定义特性增加自定义容器组件规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|119|[I4MBV7](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV7?from=project-issue)|【新增规格】滚动条样式自定义能力|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|120|[I4MBV8](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV8?from=project-issue)|【新增规格】Swiper组件新增切换禁用规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|121|[I4MBV9](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBV9?from=project-issue)|【新增规格】Tabs组件新增TabBar内容自定义规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|122|[I4MBVA](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVA?from=project-issue)|【新增规格】Navigation组件新增标题栏设置规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|123|[I4MBVB](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVB?from=project-issue)|【新增规格】工具栏组件增加工具栏显隐控制规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|124|[I4MBVC](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVC?from=project-issue)|【新增规格】工具栏组件增加内容自定义能力规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|125|[I4MBVD](https://gitee.com/openharmony/interface_sdk-js/issues/I4MBVD?from=project-issue)|【新增特性】新增SysCap声明编译特性|标准系统|SIG_ApplicationFramework|[@karl-z](https://gitee.com/karl-z)|
|126|[I4MBVE](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVE?from=project-issue)|【新增特性】新增JS SDK编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|127|[I4MBVF](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVF?from=project-issue)|【新增特性】新增Config.json编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|128|[I4MBVG](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVG?from=project-issue)|【新增规格】新增断点调试特性支持单实例调试|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|129|[I4MBVH](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVH?from=project-issue)|【新增规格】新增attach调试特性支持单实例调试|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|130|[I4MBVI](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVI?from=project-issue)|【新增规格】新增声明式范式编译特性支持编译和校验规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|131|[I4MBVJ](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVJ?from=project-issue)|【新增特性】新增JS模块共享编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|132|[I4MBVK](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVK?from=project-issue)|【新增特性】新增HAR引用和编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|133|[I4MBVL](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVL?from=project-issue)|【新增特性】新增NPM引用和编译特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|134|[I4MBVM](https://gitee.com/openharmony/docs/issues/I4MBVM?from=project-issue)|【资料】ace_engine_standard部件IT2版本资料录入需求|标准系统|SIG_Docs|[@neeen](https://gitee.com/neeen)|
|135|[I4MBVN](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVN?from=project-issue)|【新增特性】纵向显示滑动条组件特性|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|136|[I4MBVO](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVO?from=project-issue)|【新增特性】Popup组件增加内容自定义规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|137|[I4MBVP](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVP?from=project-issue)|【新增特性】Canvas绘制能力支持|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|138|[I4MBVQ](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVQ?from=project-issue)|【新增规格】Canvas能力增强|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|139|[I4MBVR](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVR?from=project-issue)|【新增特性】触摸响应热区设置|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|140|[I4MBVS](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVS?from=project-issue)|【新增特性】Lottie动画支持|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|141|[I4MBVT](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVT?from=project-issue)|【新增特性】组件尺寸获取特性|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|142|[I4MBVU](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVU?from=project-issue)|【新增特性】Menu组件增加内容自定义规格|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|143|[I4MBVV](https://gitee.com/openharmony/ace_ace_engine/issues/I4MBVV?from=project-issue)|【新增特性】Swipe手势特性|标准系统|SIG_ApplicationFramework|[@davidwulanxi](https://gitee.com/davidwulanxi)|
|144|[I4MBVW](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVW?from=project-issue)|【新增特性】UI预览支持Inspector能力|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|145|[I4MBVX](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVX?from=project-issue)|【新增特性】新增非路由文件预览特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|146|[I4MBVY](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVY?from=project-issue)|【新增特性】新增NAPI预览特性|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|147|[I4MBVZ](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBVZ?from=project-issue)|【新增规格】新增声明式范式预览特性支持基础预览规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|148|[I4MBW2](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW2?from=project-issue)|【新增规格】新增声明式范式热加载特性支持已有节点修改规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|149|[I4MBW3](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW3?from=project-issue)|【新增规格】新增声明式范式热加载特性支持新增节点规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|150|[I4MBW4](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW4?from=project-issue)|【新增规格】新增声明式范式热加载特性支持删除节点规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|151|[I4MBW5](https://gitee.com/openharmony/developtools_ace-ets2bundle/issues/I4MBW5?from=project-issue)|【新增规格】新增组件预览特性支持页面组件预览规格|标准系统|SIG_ApplicationFramework|[@lihong67](https://gitee.com/lihong67)|
|152|[I4NY1T](https://gitee.com/openharmony/device_profile_core/issues/I4NY1T?from=project-issue)|【device_profile】订阅profile信息变化|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|153|[I4NY1U](https://gitee.com/openharmony/device_profile_core/issues/I4NY1U?from=project-issue)|【device_profile】订阅同步通知|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|154|[I4NY1V](https://gitee.com/openharmony/device_profile_core/issues/I4NY1V?from=project-issue)|【device_profile】CS采集OS特征信息|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|155|[I4NY1W](https://gitee.com/openharmony/device_profile_core/issues/I4NY1W?from=project-issue)|【device_profile】向业务端提供同步profile能力|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|156|[I4NY1X](https://gitee.com/openharmony/device_profile_core/issues/I4NY1X?from=project-issue)|【device_profile】提供查询远程设备profile记录功能|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|157|[I4NY1Z](https://gitee.com/openharmony/device_profile_core/issues/I4NY1Z?from=project-issue)|【device_profile】profile上线同步（wifi组网下）|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|158|[I4NY21](https://gitee.com/openharmony/device_profile_core/issues/I4NY21?from=project-issue)|【device_profile】提供删除本地profile记录功能|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|159|[I4NY22](https://gitee.com/openharmony/device_profile_core/issues/I4NY22?from=project-issue)|【device_profile】提供查询本地profile记录功能|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|160|[I4NY23](https://gitee.com/openharmony/device_profile_core/issues/I4NY23?from=project-issue)|【device_profile】提供写入profile记录功能|标准系统|SIG_BasicSoftwareService|[@lijiarun](https://gitee.com/lijiarun)|
|161|[I4MBRC](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRC?from=project-issue)|【hiperf部件】采样数据展示|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|162|[I4MBRD](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRD?from=project-issue)|【hiperf部件】性能数据采样记录|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|163|[I4MBRE](https://gitee.com/openharmony/developtools_profiler/issues/I4MBRE?from=project-issue)|【hiperf部件】性能数据计数统计|标准系统|SIG_R&DToolChain|[@wangzaishang](https://gitee.com/wangzaishang)|
|164|[I4MBU1](https://gitee.com/openharmony/applications_settings/issues/I4MBU1?from=project-issue)|【设置公共数据存储】Settings数据管理API|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|165|[I4MBU8](https://gitee.com/openharmony/applications_settings/issues/I4MBU8?from=project-issue)|【设置】基础能力-多设备形态差异化构建|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|
|166|[I4MBU9](https://gitee.com/openharmony/applications_systemui/issues/I4MBU9?from=project-issue)|【SystemUI】【通知】通知组件化|标准系统|SIG_SystemApplication|[@lv-zhongwei](https://gitee.com/lv-zhongwei)|

## OpenHarmony 3.1.2.3版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.2.3 *****     |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第3轮测试，验收:    |
| L0L1: http  2.0协议等                                        |
| L2: 驱动能力增强相关需求                                     |
| **API****变更：**：本次转测特性不涉及API变更                 |
| **L0L1****转测试时间：2021-12-16**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:     http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165653/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165653-hispark_pegasus.tar.gz |
| hispark_taurus版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165409/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165409-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_165506/version-Master_Version-OpenHarmony_3.1.2.3-20211215_165506-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2021-12-16**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows：    http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.3/20211215_143812/version-Master_Version-Ohos_sdk_3.1.2.3-20211215_143812-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac：    https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.2.3/20211215_150037/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211215_143541/version-Master_Version-OpenHarmony_3.1.2.3-20211215_143541-hispark_taurus_L2.tar.gz |
| RK3568版本:   http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.3/20211217_181409/version-Master_Version-OpenHarmony_3.1.2.3-20211217_181409-dayu200.tar.gz |

 

**需求列表:**

| no   | issue                                                        | feture    description                                        | platform | sig                | owner                                               |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------- | ------------------ | --------------------------------------------------- |
| 1    | [I4KCO7](https://gitee.com/openharmony/build/issues/I4KCO7)  | [编译构建子系统]轻量级和标准系统支持使用统一的产品配置       | 标准系统 | SIG_CompileRuntime | [@weichaox](https://gitee.com/weichaox)             |
| 2    | [I4JQ2N](https://gitee.com/openharmony/communication_netstack/issues/I4JQ2N) | [电话服务子系统]提供Http JS API                              | 轻量系统 | SIG_Telephony      | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 3    | [I4JQ3G](https://gitee.com/openharmony/third_party_nghttp2/issues/I4JQ3G) | [电话服务子系统]提供Http 2.0协议                             | 轻量系统 | SIG_Telephony      | [@zhang-hai-feng](https://gitee.com/zhang-hai-feng) |
| 4    | [I4LZZF](https://gitee.com/openharmony/drivers_framework/issues/I4LZZF) | [驱动子系统]支持同步/异步电源管理调用                        | 标准系统 | SIG_Driver         | [@fx_zhang](https://gitee.com/fx_zhang)             |
| 5    | [I4L3LF](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3LF) | [驱动子系统]传感器驱动模型能力增强                           | 标准系统 | SIG_Driver         | [@Kevin-Lau](https://gitee.com/Kevin-Lau)           |
| 6    | [I4MBTR](https://gitee.com/openharmony/drivers_peripheral/issues/I4MBTR) | [驱动子系统]SR000GGUSG:【新增特性】Display-Layer HDI接口针对L2的参考实现； Display-Gralloc HDI接口针对L2的参考实现； Display-Device HDI接口针对L2的参考实现； | 标准系统 | SIG_Driver         | [@YUJIA](https://gitee.com/JasonYuJia)              |
| 7    | [I4D9V9](https://gitee.com/openharmony/drivers_framework/issues/I4D9V9) | [驱动子系统]hid类设备适配5.10内核                            | 标准系统 | SIG_Driver         | [@huangkai71](https://gitee.com/huangkai71)         |

## OpenHarmony 3.1.2.2版本转测试信息：

| ***\*****转测试版本号：    OpenHarmony 3.1.2.2**             |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第2轮测试，验收:    |
| L0L1: 支持Listeneer复读机                                    |
| L2: 分布式数据服务缺失功能补齐                               |
| **API****变更：**：本次转测特性不涉及API变更                 |
| **L0L1****转测试时间：2021-12-10**                           |
| **L0L1****转测试版本获取路径：**                             |
| hispark_pegasus版本:     http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_100544/version-Master_Version-OpenHarmony_3.1.2.2-20211208_100544-hispark_pegasus.tar.gz |
| hispark_taurus版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_105824/version-Master_Version-OpenHarmony_3.1.2.2-20211208_105824-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本:    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211210_092432/version-Master_Version-OpenHarmony_3.1.2.2-20211210_092432-hispark_taurus_Linux.tar.gz |
| **L2****转测试时间：2021-12-10**                             |
| **L2****转测试版本获取路径：**                               |
| hi3516dv300-L2版本 SDK linux/windows：    http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_093549/version-Master_Version-Ohos_sdk_3.1.2.2-20211208_093549-ohos-sdk.tar.gz |
| hi3516dv300-L2版本 SDK mac：    https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_095201/L2-SDK-MAC.tar.gz |
| hi3516dv300-L2版本：    http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_141157/version-Master_Version-OpenHarmony_3.1.2.2-20211208_141157-hispark_taurus_L2.tar.gz |

 

**需求列表:**

| no   | issue                                                        | feture    description                              | platform | sig                | owner                                         |
| ---- | ------------------------------------------------------------ | -------------------------------------------------- | -------- | ------------------ | --------------------------------------------- |
| 1    | [I4H3JJ](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3JJ) | L1设备分布式对象支持Listeneer复读机                | 轻量系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode)       |
| 2    | [I4IBPH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4IBPH) | 【distributed_kv_store】分布式数据服务缺失功能补齐 | 标准系统 | SIG_DataManagement | [@widecode](https://gitee.com/widecode)       |
| 3    | [I4KVJQ](https://gitee.com/openharmony/drivers_framework/issues/I4KVJQ) | [驱动子系统]支持Linux/Liteos-a内核系统级休眠唤醒   | 标准系统 | SIG_Driver         | [@fx_zhang](https://gitee.com/fx_zhang)       |
| 4    | [I4L3KK](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3KK) | [驱动子系统]传感器器件驱动能力增强                 | 标准系统 | SIG_Driver         | [@Kevin-Lau](https://gitee.com/Kevin-Lau)     |
| 5    | [I410OZ](https://gitee.com/openharmony/usb_manager/issues/I410OZ) | [USB服务子系统]轻量级和标准系统使用统一的编译流程  | 标准系统 | SIG_Driver         | [@wu-chengwen](https://gitee.com/wu-chengwen) |



## OpenHarmony 3.1.2.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.2.2              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代三第2轮测试，验收:|
|L0L1: 主要验收分布式数据对象需求                                           |
|L2: 主要验收编译构建和驱动子系统需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-12-10**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_105824/version-Master_Version-OpenHarmony_3.1.2.2-20211208_105824-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211210_092432/version-Master_Version-OpenHarmony_3.1.2.2-20211210_092432-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_100544/version-Master_Version-OpenHarmony_3.1.2.2-20211208_100544-hispark_pegasus.tar.gz |
| **L2转测试时间：2021-12-10**                                   |
 **L2转测试版本获取路径：**                                   |
 | hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_093549/version-Master_Version-Ohos_sdk_3.1.2.2-20211208_093549-ohos-sdk.tar.gz |
 | hi3516dv300-L2版本 SDK linxu/windows：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.2.2/20211208_141157/version-Master_Version-OpenHarmony_3.1.2.2-20211208_141157-hispark_taurus_L2.tar.gz|
 | hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/Ohos_sdk_3.1.2.2/20211208_095201/L2-SDK-MAC.tar.gz|

需求列表：
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1    | [I4H3JJ](https://gitee.com/openharmony/distributeddatamgr_objectstore/issues/I4H3JJ) | L1设备分布式对象支持Listeneer复读机   | 轻量系统 | SIG_DataManagement   | [@widecode](https://gitee.com/widecode)             |
| 2    | [I4IBPH](https://gitee.com/openharmony/distributeddatamgr_datamgr/issues/I4IBPH) | 【distributed_kv_store】分布式数据服务缺失功能补齐       | 标准系统 | SIG_DataManagement  | [@widecode](https://gitee.com/widecode)         |
| 3    | [I4K7E3](https://gitee.com/openharmony/build/issues/I4K7E3)  | [编译构建子系统]支持使用统一的编译命令作为编译入口      | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 4    | [I4KCMM](https://gitee.com/openharmony/build/issues/I4KCMM)  | [编译构建子系统]轻量级和标准系统使用统一的编译流程      | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 5    | [I4KCNB](https://gitee.com/openharmony/build/issues/I4KCNB)  | [编译构建子系统]支持使用统一的gn模板                   | 标准系统    | SIG_CompileRuntime   | [@weichaox](https://gitee.com/weichaox)           |
| 6    | [I4KVJQ](https://gitee.com/openharmony/drivers_framework/issues/I4KVJQ)  | [驱动子系统]支持Linux/Liteos-a内核系统级休眠唤醒      | 标准系统    | SIG_Driver  | [@fx_zhang](https://gitee.com/fx_zhang)         |
| 7    | [I4L3KK](https://gitee.com/openharmony/drivers_peripheral/issues/I4L3KK)  | [驱动子系统]传感器器件驱动能力增强               | 标准系统    | SIG_Driver  | [@Kevin-Lau](https://gitee.com/Kevin-Lau)         |
| 8    | [I410OZ](https://gitee.com/openharmony/usb_manager/issues/I410OZ)  | [USB服务子系统]轻量级和标准系统使用统一的编译流程      | 标准系统    | SIG_Driver  | [@wu-chengwen](https://gitee.com/wu-chengwen)         |



## OpenHarmony 3.1.1.3版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.3              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第3轮测试，验收:|
|L0L1: 不涉及                                           |
|L2: 主要验收帐号及软总线相关需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L2转测试时间：2021-11-22**                                   |
| **L2转测试版本获取路径**                                   |
|hi3516dv300-L2版本 SDK linux/windows：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_093209/version-Master_Version-OpenHarmony_3.1.1.3-20211122_093209-ohos-sdk.tar.gz
| hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.1.3/20211122_094743/L2-SDK-MAC.tar.gz
| hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_090334/version-Master_Version-OpenHarmony_3.1.1.3-20211122_090334-hispark_taurus_L2.tar.gz|

需求列表：
| no   | issue                                                        | feture description                         | platform | sig            | owner                                       |
| ---- | ------------------------------------------------------------ | ------------------------------------------ | -------- | -------------- | ------------------------------------------- |
| 1    | [I4FZ29](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ29) | [软总线][传输]软总线提供传输ExtAPI接口     | 标准系统 | SIG_SoftBus    | [@laosan_ted](https://gitee.com/laosan_ted) |
| 2    | [I4FZ25](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ25) | [软总线][组网]软总线支持网络切换组网       | 标准系统 | SIG_SoftBus    | [@heyingjiao](https://gitee.com/heyingjiao) |
| 3    | [I4HPR7](https://https//gitee.com/openharmony/drivers_framework/issues/I4HPR7) | [驱动子系统]提供hcs宏式解析接口            | 标准系统 | SIG_Driver     | [@fx_zhang](https://gitee.com/fx_zhang)     |
| 4    | [I4IT3U](https://gitee.com/openharmony/account_os_account/issues/I4IT3U) | [帐号子系统]支持应用帐号基础信息管理       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 5    | [I4IT4G](https://gitee.com/openharmony/account_os_account/issues/I4IT4G) | [帐号子系统]支持应用帐号信息查询           | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 6    | [I4IT4N](https://gitee.com/openharmony/account_os_account/issues/I4IT4N) | [帐号子系统]支持应用帐号功能设置与内容修改 | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 7    | [I4IT4X](https://gitee.com/openharmony/account_os_account/issues/I4IT4X) | [帐号子系统]支持应用帐号订阅及取消订阅     | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 8    | [I4IT54](https://gitee.com/openharmony/account_os_account/issues/I4IT54) | [帐号子系统]支持应用帐号的新增和删除       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |


## OpenHarmony 3.1.1.3版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.3              |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第3轮测试，验收:|
|L0L1: 不涉及                                          |
|L2: 主要验收帐号及软总线相关需求      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L2转测试时间：2021-11-22**                                   |
| **L2转测试版本获取路径**                                   |
|hi3516dv300-L2版本 SDK linux/windows：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_093209/version-Master_Version-OpenHarmony_3.1.1.3-20211122_093209-ohos-sdk.tar.gz
| hi3516dv300-L2版本 SDK mac：<br>https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.1.3/20211122_094743/L2-SDK-MAC.tar.gz
| hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.3/20211122_090334/version-Master_Version-OpenHarmony_3.1.1.3-20211122_090334-hispark_taurus_L2.tar.gz|

需求列表：
| no   | issue                                                        | feture description                         | platform | sig            | owner                                       |
| ---- | ------------------------------------------------------------ | ------------------------------------------ | -------- | -------------- | ------------------------------------------- |
| 1    | [I4FZ29](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ29) | [软总线][传输]软总线提供传输ExtAPI接口     | 标准系统 | SIG_SoftBus    | [@laosan_ted](https://gitee.com/laosan_ted) |
| 2    | [I4FZ25](https://gitee.com/openharmony/communication_dsoftbus/issues/I4FZ25) | [软总线][组网]软总线支持网络切换组网       | 标准系统 | SIG_SoftBus    | [@heyingjiao](https://gitee.com/heyingjiao) |
| 3    | [I4HPR7](https://https//gitee.com/openharmony/drivers_framework/issues/I4HPR7) | [驱动子系统]提供hcs宏式解析接口            | 标准系统 | SIG_Driver     | [@fx_zhang](https://gitee.com/fx_zhang)     |
| 4    | [I4IT3U](https://gitee.com/openharmony/account_os_account/issues/I4IT3U) | [帐号子系统]支持应用帐号基础信息管理       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 5    | [I4IT4G](https://gitee.com/openharmony/account_os_account/issues/I4IT4G) | [帐号子系统]支持应用帐号信息查询           | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 6    | [I4IT4N](https://gitee.com/openharmony/account_os_account/issues/I4IT4N) | [帐号子系统]支持应用帐号功能设置与内容修改 | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 7    | [I4IT4X](https://gitee.com/openharmony/account_os_account/issues/I4IT4X) | [帐号子系统]支持应用帐号订阅及取消订阅     | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |
| 8    | [I4IT54](https://gitee.com/openharmony/account_os_account/issues/I4IT54) | [帐号子系统]支持应用帐号的新增和删除       | 标准系统 | SIG_BscSoftSrv | [@verystone](https://gitee.com/verystone)   |


## OpenHarmony 3.1.1.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.1.2               |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第一轮测试，验收:|
|L0L1:HiStreamer相关需求，性能优化及linux版本热插拔                                            |
|L2: DFX      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-11-15**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.2/20211110_150708/version-Master_Version-OpenHarmony_3.1.1.2-20211110_150708-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.1.2/20211110_151619/version-Master_Version-OpenHarmony_3.1.1.2-20211110_151619-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony__3.1.1.2/20211110_151211/version-Master_Version-OpenHarmony_3.1.1.2-20211110_151211-hispark_pegasus.tar.gz |

需求列表:
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I4DK89](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK89) | 【需求】HiStreamer插件框架需求                             | 轻量系统 | SIG_GraphicsandMedia| [@guodongchen](https://gitee.com/guodongchen)  |
| 2   | [I4DK8D](https://gitee.com/openharmony/multimedia_histreamer/issues/I4DK8D) | 【需求】HiStreamer性能和DFX需求                            | 轻量系统 | SIG_GraphicsandMedia| [@guodongchen](https://gitee.com/guodongchen)  |
| 3   | [I3ND6Y](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ND6Y) | 【性能】OS内核&驱动启动优化                            | 轻量系统 | SIG_Kernel| [@kkup180](https://gitee.com/kkup180) |
| 4   | [I3NTCT](https://gitee.com/openharmony/startup_appspawn_lite/issues/I3NTCT) | Linux版本init支持热插拔                            | 轻量系统 | SIG_BscSoftSrv| [@handyohos](https://gitee.com/handyohos) |


## OpenHarmony 3.1.0.2版本转测试信息：
| **转测试版本号：OpenHarmony 3.1.0.2               |
| ------------------------------------------------------------ |
| **版本用途：**OpenHarmony码云master迭代二第七轮测试，验收:|
|L0L1:无                                              |
|L2: DFX      |
| **API变更：**：本次转测特性不涉及API变更                 |
| **L0L1转测试时间：2021-10-28**                                   |
| **L0L1转测试版本获取路径：**                                   |
| hispark_taurus版本：<br> http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_093735/version-Master_Version-OpenHarmony_3.1.0.2-20211027_093735-hispark_taurus_LiteOS.tar.gz |
| hispark_taurus_linux版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094506/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094506-hispark_taurus_Linux.tar.gz|
| hispark_pegasus版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094100/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094100-hispark_pegasus.tar.gz |
| **L2转测试时间：2021-10-28**                                   |
 **L2转测试版本获取路径：**                                   |
 | hi3516dv300-L2版本：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211027_094329/version-Master_Version-OpenHarmony_3.1.0.2-20211027_094329-hispark_taurus_L2.tar.gz |
 | hi3516dv300-L2版本 SDK linxu/windows：<br>http://download.ci.openharmony.cn/version/Master_Version/OpenHarmony_3.1.0.2/20211028_124231/version-Master_Version-OpenHarmony_3.1.0.2-20211028_124231-ohos-sdk.tar.gz |
 | hi3516dv300-L2版本 SDK mac：<br> https://hm-verify.obs.cn-north-4.myhuaweicloud.com/version/Master_Version/OpenHarmony_3.1.0.2/20211028_194502/L2-SDK-MAC.tar.gz|


需求列表:
| no   | issue                                                        | feture description                                           | platform     | sig                  | owner                                             |
| :--- | ------------------------------------------------------------ | :----------------------------------------------------------- | :--------- | :------------------- | :------------------------------------------------ |
| 1   | [I3XGJH](https://gitee.com/openharmony/startup_init_lite/issues/I3XGJH) | init基础环境构建                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 2   | [I3XGKV](https://gitee.com/openharmony/startup_init_lite/issues/I3XGKV) | sytemparameter管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 3   | [I3XGLN](https://gitee.com/openharmony/startup_init_lite/issues/I3XGLN) | init 脚本管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 4   | [I3XGM3](https://gitee.com/openharmony/startup_init_lite/issues/I3XGM3) | init 服务管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 5   | [I3XGMQ](https://gitee.com/openharmony/startup_init_lite/issues/I3XGMQ) | 基础权限管理                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 6   | [I3XGN8](https://gitee.com/openharmony/startup_init_lite/issues/I3XGN8) | bootimage构建和加载                             | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 7   | [I3XGO7](https://gitee.com/openharmony/startup_init_lite/issues/I3XGO7) | uevent 管理                            | 轻量系统 | SIG_BscSoftSrv| [@xionglei6](https://gitee.com/xionglei6) |
| 8   | [I4BX5Z](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX5Z) | 【需求】HiStreamer支持音频播放和控制             | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 9   | [I4BX8A](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX8A) | 【需求】HiStreamer支持常见音频格式mp3/wav的播放   | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
| 10   | [I4BX9E](https://gitee.com/openharmony/multimedia_histreamer/issues/I4BX9E) | 【需求】HiStreamer播放引擎框架需求               | 轻量系统 | SIG_GraphicsandMedia         | [@guodongchen](https://gitee.com/guodongchen) |
